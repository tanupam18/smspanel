package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class AllSummaryResultSetJdbc {

	private String DlrStatus;
	private String CountDlrStatus;

	public String getDlrStatus() {
		return DlrStatus;
	}

	public void setDlrStatus(String dlrStatus) {
		DlrStatus = dlrStatus;
	}

	public String getCountDlrStatus() {
		return CountDlrStatus;
	}

	public void setCountDlrStatus(String countDlrStatus) {
		CountDlrStatus = countDlrStatus;
	}

	public AllSummaryResultSetJdbc() {
		super();
	}

	public AllSummaryResultSetJdbc(String dlrStatus, String countDlrStatus) {
		super();
		DlrStatus = dlrStatus;
		CountDlrStatus = countDlrStatus;
	}

	@Override
	public String toString() {
		return "AllSummaryResultSetJdbc [DlrStatus=" + DlrStatus + ", CountDlrStatus=" + CountDlrStatus + "]";
	}

}
