package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class RetrieveTemplateModel {

	private String TemplateName;
	private String TemplateMessage;
	private String Sender;
	private String TemplateDltId;
	private String smsType;

	public String getTemplateName() {
		return TemplateName;
	}

	public void setTemplateName(String templateName) {
		TemplateName = templateName;
	}

	public String getTemplateMessage() {
		return TemplateMessage;
	}

	public void setTemplateMessage(String templateMessage) {
		TemplateMessage = templateMessage;
	}

	public String getSender() {
		return Sender;
	}

	public void setSender(String sender) {
		Sender = sender;
	}

	public String getTemplateDltId() {
		return TemplateDltId;
	}

	public void setTemplateDltId(String templateDltId) {
		TemplateDltId = templateDltId;
	}

	public String getSmsType() {
		return smsType;
	}

	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}

	public RetrieveTemplateModel(String templateName, String templateMessage, String sender, String templateDltId,
			String smsType) {
		super();
		TemplateName = templateName;
		TemplateMessage = templateMessage;
		Sender = sender;
		TemplateDltId = templateDltId;
		this.smsType = smsType;
	}

	public RetrieveTemplateModel() {
		super();
	}

	@Override
	public String toString() {
		return "RetrieveTemplateModel [TemplateName=" + TemplateName + ", TemplateMessage=" + TemplateMessage
				+ ", Sender=" + Sender + ", TemplateDltId=" + TemplateDltId + ", smsType=" + smsType + "]";
	}

}
