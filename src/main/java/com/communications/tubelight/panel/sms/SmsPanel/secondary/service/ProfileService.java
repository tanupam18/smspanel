package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.ProfileResponse;

@Service
public class ProfileService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public ProfileResponse profileService(String username) throws SQLException {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;
		// List<ProfileResponse> list = null;

		Sql = "Select Company, MobileNo, Name, CustomerDltId, Address from Main.Customers where username='" + username
				+ "'";

		resultSet = statement.executeQuery(Sql);

		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			System.out.println(e);
		}
		// list = new ArrayList<ProfileResponse>();
		ProfileResponse profileResponse = null;

		try {

			while (resultSet.next()) {
				profileResponse = new ProfileResponse();

				profileResponse.setCompanyName(resultSet.getString("Company"));
				profileResponse.setContactNo(resultSet.getString("MobileNo"));
				profileResponse.setAddress(resultSet.getString("Address"));
				profileResponse.setPrincipleEntityId(resultSet.getString("CustomerDltId"));
				profileResponse.setName(resultSet.getString("Name"));
				// list.add(profileResponse);

			}
			if (username.equals("durgesh")) {
				profileResponse.setRole("admin");

			} else {
				profileResponse.setRole("user");

			}
			resultSet.close();

		} catch (Exception e) {
			// TODO: handle exception
		}
		connection.close();
		return profileResponse;

	}
}
