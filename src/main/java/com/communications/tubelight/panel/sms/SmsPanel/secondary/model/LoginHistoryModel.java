package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class LoginHistoryModel {
	private String ip;
	private String createdAt;
	private String search;
	private int page;
	private int size;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public LoginHistoryModel(String ip, String createdAt, String search, int page, int size) {
		super();
		this.ip = ip;
		this.createdAt = createdAt;
		this.search = search;
		this.page = page;
		this.size = size;
	}

	public LoginHistoryModel() {
		super();
	}

	@Override
	public String toString() {
		return "LoginHistoryModel [ip=" + ip + ", createdAt=" + createdAt + ", search=" + search + ", page=" + page
				+ ", size=" + size + "]";
	}

}
