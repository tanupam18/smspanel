package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.RetreiveDownloadCenterResponse;

@Service
public class DownloadCenterRetreivingService {

	private int noOfRecords;

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	@Value("${spring.download.link}")
	private String link;

	public List<RetreiveDownloadCenterResponse> downloadRetreivingCenter(int offset, int size, String search,
			String username) throws SQLException {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;

		List<RetreiveDownloadCenterResponse> list = null;

		if (search != null && !search.isEmpty()) {
			Sql = "Select * from  DownloadLogs where username='" + username + "' AND (sender like '%" + search
					+ "%' OR RequestTime like '%" + search + "%') limit " + size;

		} else {
			Sql = "Select * from  DownloadLogs where username='" + username + "' limit " + offset + ", " + size;
		}

		System.out.println("Sql------------------------" + Sql);

		try {
			resultSet = statement.executeQuery(Sql);
		//	System.out.println("---------------------------" + resultSet.getFetchSize());
		} catch (SQLException e) {
			System.out.println(e);
		}
		list = new ArrayList<RetreiveDownloadCenterResponse>();

		// try {
		while (resultSet.next()) {
			RetreiveDownloadCenterResponse retreiveDownloadCenterResponse = new RetreiveDownloadCenterResponse();
			retreiveDownloadCenterResponse.setDownloadId(resultSet.getString("UUID"));
			//System.out.println("Uuid-----------------" + resultSet.getString("UUID"));

			retreiveDownloadCenterResponse.setUsername(resultSet.getString("username"));
		//	System.out.println("Username-----------------" + resultSet.getString("username"));

			retreiveDownloadCenterResponse.setSource(resultSet.getString("sender"));
			//System.out.println("sender-----------------" + resultSet.getString("sender"));

			retreiveDownloadCenterResponse.setFromDate(resultSet.getString("From"));
			//System.out.println("FromDate-----------------" + resultSet.getString("From"));

			retreiveDownloadCenterResponse.setToDate(resultSet.getString("To"));
			//System.out.println("ToDate-------------------" + resultSet.getString("To"));

			retreiveDownloadCenterResponse.setDataCount(resultSet.getString("DataCount"));

			retreiveDownloadCenterResponse.setRequestTime(resultSet.getString("RequestTime"));
			if (resultSet.getInt("status") == 0) {
			//	System.out.println("Status-------------------" + resultSet.getInt("status"));

				retreiveDownloadCenterResponse.setStatus("Queued");
			} else if (resultSet.getInt("status") == 1) {
				retreiveDownloadCenterResponse.setStatus("Counting");
			} else if (resultSet.getInt("status") == 2) {
				retreiveDownloadCenterResponse.setStatus("Fetching");
			} else if (resultSet.getInt("status") == 4) {
				retreiveDownloadCenterResponse.setStatus("Completed");
				retreiveDownloadCenterResponse.setLink(link);

			}

			list.add(retreiveDownloadCenterResponse);

		}

		// } catch (Exception e) {
		// System.out.println("--------------------------------" + e);
		/// }
		try {
			resultSet.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			resultSet = statement.executeQuery("Select count(*) from  DownloadLogs where username='" + username + "'");
			System.out.println("Select count(*) from  DownloadLogs where username='" + username + "'");

			if (resultSet.next())
				this.noOfRecords = resultSet.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("--------------Listing-----------" + list);
		connection.close();
		return list;
	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

}
