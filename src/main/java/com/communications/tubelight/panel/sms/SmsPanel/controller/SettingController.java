package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.communications.tubelight.panel.sms.SmsPanel.Interface.FilesStorageService;
import com.communications.tubelight.panel.sms.SmsPanel.response.BlockNumberResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.ManageSenderResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.BlockNumberModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.BlockRetreivingMultipleNumberModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.MultipleRequestModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SenderMangeModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.BlockNumberService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.ManageSenderService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MultipleBlockNumberService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.UploadFIleService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;

@RestController
@CrossOrigin
@RequestMapping("/sms/api/v1")
public class SettingController {

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	ManageSenderService manageSenderService;

	@Autowired
	BlockNumberService blockNumberService;

	@Autowired
	FilesStorageService storageService;

	@Autowired
	UploadFIleService uploadFIleService;

	@Autowired
	FilesStorageService filesStorageService;

	@Autowired
	MultipleBlockNumberService multipleBlockNumberService;

	@PostMapping("/sender/manage")
	public Map manageSender(@RequestBody SenderMangeModel senderMangeModel, @RequestHeader Map<String, String> headers)
			throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		List<ManageSenderResponse> ListManageService = null;

		try {

			ListManageService = manageSenderService.manageSenderService(
					(senderMangeModel.getPage() - 1) * senderMangeModel.getSize(), senderMangeModel.getSize(),
					senderMangeModel.getSearch(), senderMangeModel.getSender(), Username, senderMangeModel.isDelete());

		} catch (Exception e) {

			throw new ResponseStatusException(HttpStatus.CONFLICT,
					"SenderId Name already exits Please try another SenderID");
		}
		String abc = null;
		if (senderMangeModel.getSearch() != null) {
			abc = senderMangeModel.getSearch();
		} else {
			abc = "";
		}
//		if (senderMangeModel.getSender() != null && senderMangeModel.isDelete() == false && abc.isEmpty() == true
//				&& !senderMangeModel.getSender().isEmpty()) {
//			Map ValidationInsertMessage = new HashMap();
//			ValidationInsertMessage.put("message", "SenderId added successfully ");
//			return ValidationInsertMessage;
//		}

		int totalCounut = manageSenderService.getRecord();
		Map AllManageServiceSender = new HashMap();

		Map<String, String> AllUsername = new HashMap<String, String>();
		Map<String, String> AllSender = new HashMap<String, String>();
		Map<String, String> AllRequestedAt = new HashMap<String, String>();
		Map<String, String> AllStatus = new HashMap<String, String>();
		Map<String, String> AllDelete = new HashMap<String, String>();

		List<Object> AllLabel = new ArrayList<Object>();

		AllUsername.put("name", "username");
		AllUsername.put("label", "Username");

		AllSender.put("name", "sender");
		AllSender.put("label", "Sender");

		AllRequestedAt.put("name", "requestedAt");
		AllRequestedAt.put("label", "Requested At");

		AllStatus.put("name", "status");
		AllStatus.put("label", "Status");

		AllDelete.put("name", "Delete");
		AllDelete.put("label", "Delete");

		AllLabel.add(AllUsername);
		AllLabel.add(AllSender);
		AllLabel.add(AllRequestedAt);
		AllLabel.add(AllStatus);
		AllLabel.add(AllDelete);

		if (senderMangeModel.isDelete() == true) {
			Map DeleteMessage = new HashMap();
			DeleteMessage.put("message", "Sender Deleted Successfully!!!");
			DeleteMessage.put("data", ListManageService);
			DeleteMessage.put("totalCount", totalCounut);
			DeleteMessage.put("fields", AllLabel);

			return DeleteMessage;

		}

		if (senderMangeModel.getSender() != null && senderMangeModel.isDelete() == false && abc.isEmpty() == true
				&& !senderMangeModel.getSender().isEmpty()) {
			Map ValidationInsertMessage = new HashMap();
			ValidationInsertMessage.put("message", "SenderId added successfully ");
			ValidationInsertMessage.put("data", ListManageService);
			ValidationInsertMessage.put("totalCount", totalCounut);
			ValidationInsertMessage.put("fields", AllLabel);

			return ValidationInsertMessage;
		}

		AllManageServiceSender.put("data", ListManageService);
		AllManageServiceSender.put("totalCount", totalCounut); // fields
		AllManageServiceSender.put("fields", AllLabel);

		return AllManageServiceSender;
	}

	@PostMapping("/sender/block")
	public Map blockSender(@RequestHeader Map<String, String> headers, @RequestBody BlockNumberModel blockNumberModel)
			throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		List<BlockNumberResponse> ListingBlockNumber = null;
		String abc = null;

		try {
			ListingBlockNumber = blockNumberService.blockService(
					(blockNumberModel.getPage() - 1) * blockNumberModel.getSize(), blockNumberModel.getSize(),
					blockNumberModel.getSearch(), blockNumberModel.getMobileno(), Username,
					blockNumberModel.isDelete());
		} catch (SQLException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Number already Blocked Please try another Number");
		}
		if (blockNumberModel.getSearch() != null) {
			abc = blockNumberModel.getSearch();
		} else {
			abc = "";
		}

//		if (blockNumberModel.getMobileno() != null && blockNumberModel.isDelete() == false && abc.isEmpty() == true
//				&& !blockNumberModel.getMobileno().isEmpty()) {
//			Map ValidationInsertMessage = new HashMap();
//			ValidationInsertMessage.put("message", "Mobile Number added successfully");
//			return ValidationInsertMessage;
//		}

//		if (blockNumberModel.isDelete() == true) {
//			Map DeleteMessage = new HashMap();
//			DeleteMessage.put("message", "Sender Deleted Successfully!!!");
//			DeleteMessage.put("data", ListingBlockNumber);
//
//			return DeleteMessage;
//
//		}

		int totalCounut = blockNumberService.getRecord();
		Map AllBlockService = new HashMap();

		Map<String, String> AllUsername = new HashMap<String, String>();
		Map<String, String> AllMobile = new HashMap<String, String>();
		Map<String, String> AllAddedIt = new HashMap<String, String>();
		Map<String, String> DeleteIt = new HashMap<String, String>();

		List<Object> AllLabel = new ArrayList<Object>();

		AllUsername.put("name", "username");
		AllUsername.put("label", "Username");

		AllMobile.put("name", "mobile");
		AllMobile.put("label", "Mobile");

		AllAddedIt.put("name", "addedIt");
		AllAddedIt.put("label", "Added At");

		DeleteIt.put("name", "Delete");
		DeleteIt.put("label", "Delete");

		AllLabel.add(AllUsername);
		AllLabel.add(AllMobile);
		AllLabel.add(AllAddedIt);
		AllLabel.add(DeleteIt);

		if (blockNumberModel.getMobileno() != null && blockNumberModel.isDelete() == false && abc.isEmpty() == true
				&& !blockNumberModel.getMobileno().isEmpty()) {
			Map ValidationInsertMessage = new HashMap();
			ValidationInsertMessage.put("message", "Mobile Number added successfully");
			ValidationInsertMessage.put("data", ListingBlockNumber);
			ValidationInsertMessage.put("totalCount", totalCounut);
			ValidationInsertMessage.put("fields", AllLabel);

			return ValidationInsertMessage;
		}

		if (blockNumberModel.isDelete() == true) {
			Map DeleteMessage = new HashMap();
			DeleteMessage.put("message", "Sender Deleted Successfully!!!");
			DeleteMessage.put("data", ListingBlockNumber);
			DeleteMessage.put("totalCount", totalCounut);
			DeleteMessage.put("fields", AllLabel);

			return DeleteMessage;

		}

		AllBlockService.put("data", ListingBlockNumber);
		AllBlockService.put("totalCount", totalCounut); // fields
		AllBlockService.put("fields", AllLabel);

		return AllBlockService;

	}

	@PostMapping("/block/upload/numbers")
	public Map<String, String> blockMultipleNumber(@RequestHeader Map<String, String> headers,
			@RequestParam("file") MultipartFile file) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String FilePath = filesStorageService.saveBlockNumber(file, Username);

		multipleBlockNumberService.multipleBlockNumberService(FilePath, Username);
		Map<String, String> uploadMessage = new HashMap<String, String>();
		uploadMessage.put("message", "Upload file Successfully");

		return uploadMessage;

	}

	@PostMapping("/block/retrieve/numbers")
	public Map blockMultipleRetreivingNumber(@RequestHeader Map<String, String> headers,
			@RequestBody BlockNumberModel blockNumberModel) {

		List<BlockNumberResponse> ListingBlockNumber = null;

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		blockNumberService.preventInsertionMultipleBlock();

		try {
			ListingBlockNumber = blockNumberService.blockService(
					(blockNumberModel.getPage() - 1) * blockNumberModel.getSize(), blockNumberModel.getSize(),
					blockNumberModel.getSearch(), blockNumberModel.getMobileno(), Username,
					blockNumberModel.isDelete());
		} catch (SQLException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Number already Blocked Please try another Number");
		}

		int totalCounut = blockNumberService.getRecord();
		Map AllBlockService = new HashMap();

		Map<String, String> AllUsername = new HashMap<String, String>();
		Map<String, String> AllMobile = new HashMap<String, String>();
		Map<String, String> AllAddedIt = new HashMap<String, String>();

		List<Object> AllLabel = new ArrayList<Object>();

		AllUsername.put("name", "username");
		AllUsername.put("label", "Username");

		AllMobile.put("name", "mobile");
		AllMobile.put("label", "Mobile");

		AllAddedIt.put("name", "addedIt");
		AllAddedIt.put("label", "Added At");

		AllLabel.add(AllUsername);
		AllLabel.add(AllMobile);
		AllLabel.add(AllAddedIt);

		if (blockNumberModel.isDelete() == true) {
			Map DeleteMessage = new HashMap();
			DeleteMessage.put("message", "Sender Deleted Successfully!!!");
			DeleteMessage.put("data", ListingBlockNumber);
			DeleteMessage.put("totalCount", totalCounut);
			DeleteMessage.put("fields", AllLabel);

			return DeleteMessage;

		}

		System.out.println("check kr bhai " + blockNumberService.preventInsertionMultipleBlock());

		AllBlockService.put("data", ListingBlockNumber);
		AllBlockService.put("totalCount", totalCounut); // fields
		AllBlockService.put("fields", AllLabel);

		return AllBlockService;

	}

}
