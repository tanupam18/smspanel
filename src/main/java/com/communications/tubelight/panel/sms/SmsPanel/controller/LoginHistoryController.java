package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.communications.tubelight.panel.sms.SmsPanel.response.LoginHistoryResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.LoginHistoryModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.LoginHistoryService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
public class LoginHistoryController {

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	LoginHistoryService loginHistoryService;

	@PostMapping("/login/history")
	public Map loginHistory(@RequestHeader Map<String, String> headers,
			@RequestBody LoginHistoryModel loginHistoryModel) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		List<LoginHistoryResponse> LoginHistoryData = loginHistoryService.loginHistoryService(
				(loginHistoryModel.getPage() - 1) * loginHistoryModel.getSize(), loginHistoryModel.getSize(),
				loginHistoryModel.getSearch(), Username);

		Map AllLoginHistory = new HashMap();

		int totalCount = loginHistoryService.getNoOfRecords();

		Map<String, String> AllUsername = new HashMap<String, String>();
		Map<String, String> AllIp = new HashMap<String, String>();
		Map<String, String> AllCreated = new HashMap<String, String>();

		AllUsername.put("name", "username");
		AllUsername.put("label", "Username");

		AllIp.put("name", "ip");
		AllIp.put("label", "IP");

		AllCreated.put("name", "created");
		AllCreated.put("label", "Created");

		List AddAllLabel = new ArrayList();

		AddAllLabel.add(AllUsername);
		AddAllLabel.add(AllIp);
		AddAllLabel.add(AllCreated);

		AllLoginHistory.put("data", LoginHistoryData);
		AllLoginHistory.put("totalCount", totalCount);
		AllLoginHistory.put("fields", AddAllLabel);

		return AllLoginHistory;
	}

}
