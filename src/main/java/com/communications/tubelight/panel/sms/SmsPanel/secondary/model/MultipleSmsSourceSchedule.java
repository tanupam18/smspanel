package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class MultipleSmsSourceSchedule {

	private boolean ScheduleDate;
	private long datePicker;
	//private String timePicker;
	public boolean isScheduleDate() {
		return ScheduleDate;
	}
	public void setScheduleDate(boolean scheduleDate) {
		ScheduleDate = scheduleDate;
	}
	public long getDatePicker() {
		return datePicker;
	}
	public void setDatePicker(long datePicker) {
		this.datePicker = datePicker;
	}
	public MultipleSmsSourceSchedule(boolean scheduleDate, long datePicker) {
		super();
		ScheduleDate = scheduleDate;
		this.datePicker = datePicker;
	}
	
	
	public MultipleSmsSourceSchedule() {
		super();
	}
	@Override
	public String toString() {
		return "MultipleSmsSourceSchedule [ScheduleDate=" + ScheduleDate + ", datePicker=" + datePicker + "]";
	}

	
}
