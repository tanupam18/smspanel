package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.communications.tubelight.panel.sms.SmsPanel.response.CreditLogResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.DataStatisticResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.DescriptiveSummaryResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.JobSummaryResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.MonthlyStatistic;
import com.communications.tubelight.panel.sms.SmsPanel.response.RetreiveDownloadCenterResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.SenderStatsticResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.BulkJobResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.ScheduleBulkJobResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SmsLogResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.DownlooadCsvService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MobileLogService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MobileLogsServices;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.BulkJobDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.CreditLogsDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.DatewiseSummaryDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.DescriptiveSummaryDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.DownloadCenterCsvService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.JobSummaryServiceDownload;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.MobileLogDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.MonthwiseSummaryDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.ScheduleJobDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.SenderwiseDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download.SmsLogDownloadService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;
import com.google.common.net.HttpHeaders;
import com.opencsv.CSVWriter;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
public class DownloadController {

	@Autowired
	DownlooadCsvService downlooadCsvService;

	@Autowired
	SmsLogDownloadService smsLogDownloadService;

	@Autowired
	BulkJobDownloadService bulkJobDownloadService;

	@Autowired
	ScheduleJobDownloadService scheduleJobDownloadService;

	@Autowired
	MobileLogDownloadService mobileLogDownloadService;

	@Autowired
	CreditLogsDownloadService creditLogsDownloadService;
	@Autowired
	DownloadCenterCsvService downloadCenterCsvService;

	@Autowired
	DatewiseSummaryDownloadService datewiseSummaryDownloadService;

	@Autowired
	MonthwiseSummaryDownloadService monthwiseSummaryDownloadService;

	@Autowired
	SenderwiseDownloadService senderwiseDownloadService;

	@Autowired
	DescriptiveSummaryDownloadService descriptiveSummaryDownloadService;

	@Autowired
	JobSummaryServiceDownload jobSummaryServiceDownload;

	@Autowired
	MobileLogDownloadService mobileLogService;

	@Autowired
	JwtUtils jwtUtils;

	@Value("${spring.upload.location}")
	private String Link;
	
	@Value("${spring.download.url}")
	private String downloadLink;

	@GetMapping("multiple/sms/download")
	public ResponseEntity<InputStreamResource> multipleCsvLoad(
			@RequestHeader(name = "Content-disposition") final String fileName,
			@RequestHeader(name = "Content-Type") final String mediaType) {

		String[] MobileNo = { "9999999999", "8888888888", "7777777777", "6666666666" };

		downlooadCsvService.multipleDownloadCsvService(MobileNo);

		final InputStreamResource resource = new InputStreamResource(downlooadCsvService.multipleLoad(MobileNo));
		System.out.println("--------------------------------" + fileName);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, fileName)
				.contentType(MediaType.parseMediaType(mediaType)).body(resource);

	}

	@GetMapping("/sms/log/download")
	public Map<String, String> SmsLog(@RequestHeader Map<String, String> headers) throws MalformedURLException {
		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String csvFileName = "books.csv";

		// response.setContentType("text/csv");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		// response.setHeader(headerKey, headerValue);

		List<SmsLogResponse> list = smsLogDownloadService.smsLogDownload(Username);
		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "Source", "destination", "type", "Message", "Length", "Count", "SubmitTime",
				"DeliveryTime", "Id", "status" };
		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		//String ProccessedFilePath1 = "/home/dfiles/" + Timestamp + "sms_logs.csv";
		String ProccessedFilePath = Link + Timestamp + "sms_logs.csv";
		//System.out.println("ProccessedFilePath1----------------"+ProccessedFilePath1);
		System.out.println("ProccessedFilePath----------------"+ProccessedFilePath);

		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (SmsLogResponse smsLogResponse : list) {

				data = Arrays.asList(smsLogResponse.getSource(), smsLogResponse.getDestination(),
						smsLogResponse.getType(), smsLogResponse.getMessage(), smsLogResponse.getLength(),
						smsLogResponse.getCount(), smsLogResponse.getSubmitTime(), smsLogResponse.getDeliveryTime(),
						smsLogResponse.getId(), smsLogResponse.getStatus());

				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));
			System.out.println("SmsLog Before------------"+"http://43.205.226.148/dfiles/" + Timestamp + "sms_logs.csv");
			System.out.println("SmsLog After------------"+ downloadLink + Timestamp + "sms_logs.csv");

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "sms_logs.csv");
			return FileInformation;

		} catch (IOException e) {
			System.out.println(e);

			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/bulk/job/download")
	public Map<String, String> BulkJob(@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String csvFileName = "books.csv";

		// response.setContentType("text/csv");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		// response.setHeader(headerKey, headerValue);

		List<BulkJobResponse> list = bulkJobDownloadService.bulkJob(Username);
		List<Object> data = null;
		System.out.println("---------------" + list.toString());
		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "SUMMARY", "CAMPAIGN / FILE NAME", "MESSAGE", "LENGTH", "T.SENT", "T.COUNT", "SENDER",
				"QUEUED AT", "COMPLETED AT", "STATUS" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "bulk_job.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (BulkJobResponse bulkJobResponse : list) {

				data = Arrays.asList(bulkJobResponse.getSummary(), bulkJobResponse.getFileName(),
						bulkJobResponse.getMessage(), bulkJobResponse.getLength(), bulkJobResponse.getTsent(),
						bulkJobResponse.getTsent(), bulkJobResponse.getTcount(), bulkJobResponse.getSender(),
						bulkJobResponse.getQueuedAt(), bulkJobResponse.getCompletedAt(), bulkJobResponse.getStatus());

				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "bulk_job.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/schedule/job/download")
	public Map<String, String> scheduleJob(@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String csvFileName = "books.csv";

		// response.setContentType("text/csv");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		// response.setHeader(headerKey, headerValue);

		List<ScheduleBulkJobResponse> list = scheduleJobDownloadService.smslog(Username);
		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "SUMMARY", "CAMPAIGN / FILE NAME", "MESSAGE", "LENGTH", "T.SENT", "T.COUNT", "SENDER",
				"QUEUED AT", "COMPLETED AT", "STATUS", "ACTION" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "schedule_job.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (ScheduleBulkJobResponse scheduleBulkJobResponse : list) {

				data = Arrays.asList(scheduleBulkJobResponse.getSummary(), scheduleBulkJobResponse.getFileName(),
						scheduleBulkJobResponse.getMessage(), scheduleBulkJobResponse.getLength(),
						scheduleBulkJobResponse.getTsent(), scheduleBulkJobResponse.getTcount(),
						scheduleBulkJobResponse.getSender(), scheduleBulkJobResponse.getQueuedAt(),
						scheduleBulkJobResponse.getCompletedAt(), scheduleBulkJobResponse.getStatus(),
						scheduleBulkJobResponse.getAction());

				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "schedule_job.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

//TODO
	@GetMapping("/mobile/logs/download")
	public Map<String, String> mobileLogs(@RequestHeader Map<String, String> headers,
			@RequestParam("date") long date, @RequestParam("mobile") String mobile) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String csvFileName = "books.csv";

		// response.setContentType("text/csv");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		// response.setHeader(headerKey, headerValue);

		Date fromDate = new Date(date);
		DateFormat fromDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String fromDateformatted = fromDateFormat.format(fromDate);

		List<SmsLogResponse> list = null;
		try {
			list = mobileLogService.mobileLogs(fromDateformatted, mobile, Username);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "Source", "Destination", "Type", "Message", "Length", "Count", "Submit Time",
				"Delivery Time", "ID", "Status" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "mobile_logs.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (SmsLogResponse smsLogResponse : list) {

				data = Arrays.asList(smsLogResponse.getSource(), smsLogResponse.getDestination(),
						smsLogResponse.getType(), smsLogResponse.getMessage(), smsLogResponse.getLength(),
						smsLogResponse.getCount(), smsLogResponse.getSubmitTime(), smsLogResponse.getDeliveryTime(),
						smsLogResponse.getId(), smsLogResponse.getStatus());

				printer.printRecord(data);

			}
			
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "mobile_logs.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/credit/logs/download")
	public Map<String, String> creditLogs(@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String csvFileName = "books.csv";

		// response.setContentType("text/csv");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		// response.setHeader(headerKey, headerValue);

		List<CreditLogResponse> list = creditLogsDownloadService.creditLogService(Username);

		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "ADJUSTED AT", "USERNAME", "NEW CREDIT ALLOTED", "OLD CREDIT ALLOTED", "NEW BALANCE",
				"OLD BALANCE", "ADJUSTMENT" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "credit_logs.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (CreditLogResponse creditLogResponse : list) {

				data = Arrays.asList(creditLogResponse.getAdjustedAt(), creditLogResponse.getUsername(),
						creditLogResponse.getNewCreditAlloted(), creditLogResponse.getOldCreditAlloted(),
						creditLogResponse.getNewBalance(), creditLogResponse.getOldBalance(),
						creditLogResponse.getAdjustment());

				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "credit_logs.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/center/download")
	public Map<String, String> downloadCenter(@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String csvFileName = "books.csv";

		// response.setContentType("text/csv");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		// response.setHeader(headerKey, headerValue);

		List<RetreiveDownloadCenterResponse> list = downloadCenterCsvService.downloadRetreivingCenter(Username);

		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "DOWNLOAD ID", "USERNAME", "SOURCE", "FROM", "TO", "DATA COUNT", "REQUEST TIME",
				"STATUS" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "download_center.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (RetreiveDownloadCenterResponse retreiveDownloadCenterResponse : list) {
				data = Arrays.asList(retreiveDownloadCenterResponse.getDownloadId(),
						retreiveDownloadCenterResponse.getUsername(), retreiveDownloadCenterResponse.getSource(),
						retreiveDownloadCenterResponse.getFromDate(), retreiveDownloadCenterResponse.getToDate(),
						retreiveDownloadCenterResponse.getDataCount(), retreiveDownloadCenterResponse.getRequestTime(),
						retreiveDownloadCenterResponse.getStatus());

				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "download_center.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/summary/datewise/download")
	public Map<String, String> datewiseSummary(@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);
		// response.setContentType("text/csv");

		List<DataStatisticResponse> list = datewiseSummaryDownloadService.datewisestatistic(Username);

		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "SENDER", "DATE", "STATUS", "COUNT" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "datewise_summary.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (DataStatisticResponse dataStatisticResponse : list) {
				data = Arrays.asList(dataStatisticResponse.getSender(), dataStatisticResponse.getDate(),
						dataStatisticResponse.getStatus(), dataStatisticResponse.getCount());
				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "datewise_summary.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/summary/monthly/download")
	public Map<String, String> monthlySummary(@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);
		// response.setContentType("text/csv");

		List<MonthlyStatistic> list = monthwiseSummaryDownloadService.monthwiseStatistic(Username);

		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "DATE", "SUBMISSION", "DELIVERED", "UNDELIVERED", "EXPIRED", "REJECTED", "DND", "PENDING",
				"OTHERS" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "monthly_summary.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (MonthlyStatistic monthlyStatistic : list) {
				data = Arrays.asList(monthlyStatistic.getDate(), monthlyStatistic.getSubmission(),
						monthlyStatistic.getDelivered(), monthlyStatistic.getUndelivered(),
						monthlyStatistic.getExpired(), monthlyStatistic.getRejected(), monthlyStatistic.getDnd(),
						monthlyStatistic.getPending(), monthlyStatistic.getOther());
				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "monthly_summary.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/summary/sender/download")
	public Map<String, String> senderSummary(@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		List<SenderStatsticResponse> list = senderwiseDownloadService.senderwiseStatistic(Username);

		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "SENDER", "SUBMISSION", "DELIVERED", "UNDELIVERED", "EXPIRED", "REJECTED", "DND",
				"PENDING", "OTHERS" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "sender_summary.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (SenderStatsticResponse senderStatsticResponse : list) {
				data = Arrays.asList(senderStatsticResponse.getSender(), senderStatsticResponse.getSubmission(),
						senderStatsticResponse.getDelivered(), senderStatsticResponse.getUndelivered(),
						senderStatsticResponse.getExpired(), senderStatsticResponse.getRejected(),
						senderStatsticResponse.getDnd(), senderStatsticResponse.getPending(),
						senderStatsticResponse.getOthers());
				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "sender_summary.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/summary/descriptive/download")
	public Map<String, String> descriptiveSummary(@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		List<DescriptiveSummaryResponse> list = descriptiveSummaryDownloadService.discriptiveSummary(Username);

		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "DLS STATUS", "ERROR DISCRIPTION", "ERROR CODE", "TOTAL COUNT" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "descriptive_summary.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (DescriptiveSummaryResponse descriptiveSummaryResponse : list) {
				data = Arrays.asList(descriptiveSummaryResponse.getDlsStatus(),
						descriptiveSummaryResponse.getErrorDiscription(), descriptiveSummaryResponse.getErrorCode(),
						descriptiveSummaryResponse.getTotalCount());
				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "descriptive_summary.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@GetMapping("/summary/job/download")
	public Map<String, String> jobSummary(@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		List<JobSummaryResponse> list = jobSummaryServiceDownload.jobSummary(Username);

		List<Object> data = null;

		// ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
		// CsvPreference.STANDARD_PREFERENCE);
		String[] HEADERS = { "DATE", "JOBID", "FILE", "SMS", "TOTAL", "DELIVERED" };

		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);

//			csvWriter.writeHeader(HEADERS);
		// InputStreamResource resource = null;
		// FileWriter fileWriter = null;
		String Timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String ProccessedFilePath = Link + Timestamp + "job_summary.csv";
		// System.out.println(new File(System.getProperty("user.dir") +
		// "/anupam.csv").toURI().toURL());

		try (// final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				FileWriter fileWriter = new FileWriter(ProccessedFilePath);
				// final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), FORMAT)) {
				final CSVPrinter printer = new CSVPrinter(fileWriter, FORMAT)) {

			// SVFormat csvFileFormat =
			// CSVFormat.DEFAULT.withRecordSeparator(Defines.NEW_LINE_SEPARATOR);

			// printer = new CSVPrinter(fileWriter, FORMAT);
			// printer.printRecord(Defin);

			for (JobSummaryResponse jobSummaryResponse : list) {
				data = Arrays.asList(jobSummaryResponse.getDate(), jobSummaryResponse.getJobid(),
						jobSummaryResponse.getFile(), jobSummaryResponse.getSms(), jobSummaryResponse.getTotal(),
						jobSummaryResponse.getDelivered());
				printer.printRecord(data);

			}
			printer.flush();
			// new ByteArrayInputStream(stream.toByteArray());
			// resource = new InputStreamResource(new
			// ByteArrayInputStream(stream.toByteArray()));

			Map<String, String> FileInformation = new HashMap<String, String>();
			FileInformation.put("status", "success");
			FileInformation.put("fileurl", downloadLink + Timestamp + "job_summary.csv");
			return FileInformation;

		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

}
