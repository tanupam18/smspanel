package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class MultipleSmsSource {
	private String CampaignName;
	private String sender;
	private String smsType;
	private String Message;
	private Long fromDate;
	private String fileName;
	private Integer totalNumber;
	private String TemplateId;
	private String PeId;

	public String getTemplateId() {
		return TemplateId;
	}

	public void setTemplateId(String templateId) {
		TemplateId = templateId;
	}

	public String getPeId() {
		return PeId;
	}

	public void setPeId(String peId) {
		PeId = peId;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}

	public String getCampaignName() {
		return CampaignName;
	}

	public void setCampaignName(String campaignName) {
		CampaignName = campaignName;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSmsType() {
		return smsType;
	}

	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public Long getFromDate() {
		return fromDate;
	}

	public void setFromDate(Long fromDate) {
		this.fromDate = fromDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MultipleSmsSource(String campaignName, String sender, String smsType, String message, Long fromDate,
			String fileName, Integer totalNumber, String templateId, String peId) {
		super();
		CampaignName = campaignName;
		this.sender = sender;
		this.smsType = smsType;
		Message = message;
		this.fromDate = fromDate;
		this.fileName = fileName;
		this.totalNumber = totalNumber;
		TemplateId = templateId;
		PeId = peId;
	}

	public MultipleSmsSource() {
		super();
	}

	@Override
	public String toString() {
		return "MultipleSmsSource [CampaignName=" + CampaignName + ", sender=" + sender + ", smsType=" + smsType
				+ ", Message=" + Message + ", fromDate=" + fromDate + ", fileName=" + fileName + ", totalNumber="
				+ totalNumber + ", TemplateId=" + TemplateId + ", PeId=" + PeId + "]";
	}

}
