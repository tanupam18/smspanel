package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.RetrieveTemplateModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SendTemplateModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.TemplateRetrievingRequest;

@Service
public class TemplateRetrieveService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public ArrayList<RetrieveTemplateModel> retrieveTemplate(String username, String search) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {

		}

		ResultSet resultSet = null;
		ArrayList<RetrieveTemplateModel> SummaryResultSet = null;
		try {

			if (search != null && !search.isEmpty()) {

				resultSet = statement.executeQuery(
						"Select TempName,Ttype, Template, SenderId, TemplateDltId from Template where Username='"
								+ username + "' AND ( TempName like '%" + search + "%' OR Template like '%" + search
								+ "%')");
				System.out.println(
						"Select TempName,Ttype, Template, SenderId, TemplateDltId from Template where Username='"
								+ username + "' AND ( TempName like '%" + search + "%' OR Template like '%" + search
								+ "%')");
			} else {
				resultSet = statement.executeQuery(
						"Select TempName,Ttype, Template, SenderId, TemplateDltId from Template where Username='"
								+ username + "'");
				System.out.println(
						"Select TempName,Ttype, Template, SenderId, TemplateDltId from Template where Username='"
								+ username + "'");

			}

			String TemplateName;
			String TemplateMessage;
			String Sender;
			String TemplateDltId;
			String smsType;

			SummaryResultSet = new ArrayList<>();

			while (resultSet.next()) {

				TemplateName = resultSet.getString("TempName").toString();
				TemplateMessage = resultSet.getString("Template");
				Sender = resultSet.getString("SenderId");
				TemplateDltId = resultSet.getString("TemplateDltId");
				smsType = resultSet.getString("Ttype");

				RetrieveTemplateModel entity = new RetrieveTemplateModel(TemplateName, TemplateMessage, Sender,
						TemplateDltId, smsType);

				SummaryResultSet.add(entity);
			}

			connection.close();
		} catch (SQLException e) {

		}

		return SummaryResultSet;

	}

}
