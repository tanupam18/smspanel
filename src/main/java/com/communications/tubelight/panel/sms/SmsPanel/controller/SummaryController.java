package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.communications.tubelight.panel.sms.SmsPanel.response.DataStatisticResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.DescriptiveSummaryResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.JobSummaryResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.MonthlyStatistic;
import com.communications.tubelight.panel.sms.SmsPanel.response.SenderStatsticResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.DatestatisticModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.DescriptiveSummaryModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SenderWiseStatisticModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.DatewisestatisticService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.DiscriptiveSummaryService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.JobSummaryService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MonthwiseStatisticService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.SenderwiseStatisticService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
public class SummaryController {

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	DatewisestatisticService datewisestatisticService;

	@Autowired
	MonthwiseStatisticService monthwiseStatisticService;

	@Autowired
	JobSummaryService jobSummaryService;

	@Autowired
	SenderwiseStatisticService senderwiseStatisticService;

	@Autowired
	DiscriptiveSummaryService discriptiveSummaryService;

	@PostMapping("/date/statistic")
	public Map datewiseStatistic(@RequestBody DatestatisticModel datestatisticModel,
			@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		Map SummaryDatewise = new HashMap();

		Map<String, Object> AllLebelSender = new HashMap<String, Object>();
		Map<String, Object> AllLebelDate = new HashMap<String, Object>();
		Map<String, Object> AllLebelStatus = new HashMap<String, Object>();
		Map<String, Object> AllLebelCount = new HashMap<String, Object>();

		AllLebelSender.put("name", "sender");
		AllLebelSender.put("label", "Sender");

		AllLebelDate.put("name", "date");
		AllLebelDate.put("label", "Date");

		AllLebelStatus.put("name", "status");
		AllLebelStatus.put("label", "Status");

		AllLebelCount.put("name", "count");
		AllLebelCount.put("label", "Count");

		List AddAllLabel = new ArrayList();

		AddAllLabel.add(AllLebelSender);
		AddAllLabel.add(AllLebelDate);
		AddAllLabel.add(AllLebelStatus);
		AddAllLabel.add(AllLebelCount);

		List<DataStatisticResponse> dateWiseStatisitic = datewisestatisticService.datewisestatistic(
				(datestatisticModel.getPage() - 1) * datestatisticModel.getSize(), datestatisticModel.getSize(),
				datestatisticModel.getSearch(), Username, datestatisticModel.getSender());

		int totalCount = datewisestatisticService.getNoOfRecords();
		System.out.println("totalCount--------------" + totalCount);

		SummaryDatewise.put("data", dateWiseStatisitic);
		SummaryDatewise.put("totalCount", totalCount);
		SummaryDatewise.put("fields", AddAllLabel);

		return SummaryDatewise;

	}

	@PostMapping("/month/statistic")
	public Map monthlyStatistic(@RequestBody DatestatisticModel datestatisticModel,
			@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);
		Map SummaryDatewise = new HashMap();

		Map SummaryMonthwise = new HashMap();

		Map<String, Object> date = new HashMap<String, Object>();
		Map<String, Object> submission = new HashMap<String, Object>();
		Map<String, Object> delivered = new HashMap<String, Object>();
		Map<String, Object> undelivered = new HashMap<String, Object>();
		Map<String, Object> expired = new HashMap<String, Object>();
		Map<String, Object> rejected = new HashMap<String, Object>();
		Map<String, Object> dnd = new HashMap<String, Object>();
		Map<String, Object> pending = new HashMap<String, Object>();
		Map<String, Object> others = new HashMap<String, Object>();

		date.put("name", "date");
		date.put("label", "Date");

		submission.put("name", "submission");
		submission.put("label", "Submission");

		delivered.put("name", "delivered");
		delivered.put("label", "Delivered");

		undelivered.put("name", "undelivered");
		undelivered.put("label", "Undelivered");

		expired.put("name", "expired");
		expired.put("label", "Expired");

		rejected.put("name", "rejected");
		rejected.put("label", "Rejected");

		dnd.put("name", "dnd");
		dnd.put("label", "Dnd");

		pending.put("name", "pending");
		pending.put("label", "Pending");

		others.put("name", "other");
		others.put("label", "Others");

		List AddAllLabel = new ArrayList();

		AddAllLabel.add(date);
		AddAllLabel.add(submission);
		AddAllLabel.add(delivered);
		AddAllLabel.add(undelivered);
		AddAllLabel.add(expired);
		AddAllLabel.add(rejected);
		AddAllLabel.add(dnd);
		AddAllLabel.add(pending);
		AddAllLabel.add(others);

		List<MonthlyStatistic> monthwise = monthwiseStatisticService.monthwiseStatistic(
				(datestatisticModel.getPage() - 1) * datestatisticModel.getSize(), datestatisticModel.getSize(),
				datestatisticModel.getSearch(), Username, datestatisticModel.getSender());
		int totalCount = monthwiseStatisticService.getNoOfRecords();
		SummaryDatewise.put("data", monthwise);
		SummaryDatewise.put("totalCount", totalCount);
		SummaryDatewise.put("fields", AddAllLabel);

		return SummaryDatewise;

	}

	@PostMapping("/jobsummary")
	public Map JobSummary(@RequestBody DatestatisticModel datestatisticModel,
			@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		Map AllJobSummary = new HashMap();

		Map<String, Object> date = new HashMap<String, Object>();
		Map<String, Object> jobid = new HashMap<String, Object>();
		Map<String, Object> file = new HashMap<String, Object>();
		Map<String, Object> sms = new HashMap<String, Object>();
		Map<String, Object> total = new HashMap<String, Object>();
		Map<String, Object> delivered = new HashMap<String, Object>();

		date.put("name", "date");
		date.put("label", "Date");

		jobid.put("name", "jobid");
		jobid.put("label", "JOBID");

		file.put("name", "file");
		file.put("label", "FILE");

		sms.put("name", "message");
		sms.put("label", "SMS");

		total.put("name", "total");
		total.put("label", "TOTAL");

		delivered.put("name", "delivered");
		delivered.put("label", "DELIVERED");

		List AddAllLabel = new ArrayList();

		AddAllLabel.add(date);
		AddAllLabel.add(jobid);
		AddAllLabel.add(file);
		AddAllLabel.add(sms);
		AddAllLabel.add(total);
		AddAllLabel.add(delivered);

		List<JobSummaryResponse> JobSummaryData = jobSummaryService.jobSummary(
				(datestatisticModel.getPage() - 1) * datestatisticModel.getSize(), datestatisticModel.getSize(),
				datestatisticModel.getSearch(), Username, datestatisticModel.getSender());
		int totalCount = jobSummaryService.getNoOfRecords();

		AllJobSummary.put("data", JobSummaryData);
		AllJobSummary.put("totalCount", totalCount);
		AllJobSummary.put("fields", AddAllLabel);

		return AllJobSummary;

	}

	@PostMapping("/sender/statistic")
	public Map senderStatistic(@RequestHeader Map<String, String> headers,
			@RequestBody SenderWiseStatisticModel senderWiseStatisticModel) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		Map SenderStatics = new HashMap();

		Map<String, Object> sender = new HashMap<String, Object>();
		Map<String, Object> submission = new HashMap<String, Object>();
		Map<String, Object> delivered = new HashMap<String, Object>();
		Map<String, Object> undelivered = new HashMap<String, Object>();
		Map<String, Object> expired = new HashMap<String, Object>();
		Map<String, Object> rejected = new HashMap<String, Object>();
		Map<String, Object> dnd = new HashMap<String, Object>();
		Map<String, Object> pending = new HashMap<String, Object>();
		Map<String, Object> others = new HashMap<String, Object>();

		sender.put("name", "sender");
		sender.put("label", "Sender");

		submission.put("name", "submission");
		submission.put("label", "Submission");

		delivered.put("name", "delivered");
		delivered.put("label", "Delivered");

		undelivered.put("name", "undelivered");
		undelivered.put("label", "Undelivered");

		expired.put("name", "expired");
		expired.put("label", "Expired");

		rejected.put("name", "rejected");
		rejected.put("label", "Rejected");

		dnd.put("name", "Dnd");
		dnd.put("label", "dnd");

		pending.put("name", "pending");
		pending.put("label", "Pending");

		others.put("name", "others");
		others.put("label", "Others");

		List AddAllLabel = new ArrayList();

		AddAllLabel.add(sender);
		AddAllLabel.add(submission);
		AddAllLabel.add(delivered);
		AddAllLabel.add(undelivered);
		AddAllLabel.add(expired);
		AddAllLabel.add(rejected);
		AddAllLabel.add(dnd);
		AddAllLabel.add(pending);
		AddAllLabel.add(others);

		Date fromDate = new Date(senderWiseStatisticModel.getFromDate());
		DateFormat fromDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date toDate = new Date(senderWiseStatisticModel.getToDate());
		DateFormat toDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String fromDateformatted = fromDateFormat.format(fromDate);
		String toDateformatted = toDateFormat.format(toDate);

		List<SenderStatsticResponse> senderStatisticsData = senderwiseStatisticService.senderwiseStatistic(
				(senderWiseStatisticModel.getPage() - 1) * senderWiseStatisticModel.getSize(),
				senderWiseStatisticModel.getSize(), senderWiseStatisticModel.getSearch(), Username, fromDateformatted,
				toDateformatted);
		int totalCount = senderwiseStatisticService.getNoOfRecords();

		SenderStatics.put("data", senderStatisticsData); // totalCount
		if (!senderStatisticsData.isEmpty()) {
			SenderStatics.put("totalCount", totalCount);

		}else {
			SenderStatics.put("totalCount", 0);
		}
		SenderStatics.put("fields", AddAllLabel);

		return SenderStatics;
	}

	@PostMapping("/descriptive/summary")
	public Map descriptiveSummary(@RequestBody DescriptiveSummaryModel descriptiveSummaryModel,
			@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		Date fromDate = new Date(descriptiveSummaryModel.getFromDate());
		DateFormat fromDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date toDate = new Date(descriptiveSummaryModel.getToDate());
		DateFormat toDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String fromDateformatted = fromDateFormat.format(fromDate);
		String toDateformatted = toDateFormat.format(toDate);

		List<DescriptiveSummaryResponse> discriptiveSummaryData = discriptiveSummaryService.discriptiveSummary(
				(descriptiveSummaryModel.getPage() - 1) * descriptiveSummaryModel.getSize(),
				descriptiveSummaryModel.getSize(), descriptiveSummaryModel.getSearch(), Username, fromDateformatted,
				toDateformatted);

		Map descriptiveSummary = new HashMap();
		int totalCount = discriptiveSummaryService.getNoOfRecords();

		Map<String, Object> dlsStatus = new HashMap<String, Object>();
		Map<String, Object> errorDescription = new HashMap<String, Object>();
		Map<String, Object> errorCode = new HashMap<String, Object>();
		Map<String, Object> totalCounts = new HashMap<String, Object>();

		dlsStatus.put("name", "dlsStatus");
		dlsStatus.put("label", "DLS Status");

		errorDescription.put("name", "errorDiscription");
		errorDescription.put("label", "Error Discription");

		errorCode.put("name", "errorCode");
		errorCode.put("label", "Error Code");

		totalCounts.put("name", "totalCount");
		totalCounts.put("label", "Total Count");

		List AddAllLabel = new ArrayList();

		AddAllLabel.add(dlsStatus);
		AddAllLabel.add(errorDescription);
		AddAllLabel.add(errorCode);
		AddAllLabel.add(totalCounts);

		descriptiveSummary.put("data", discriptiveSummaryData);
		descriptiveSummary.put("totalCount", totalCount);
		descriptiveSummary.put("fields", AddAllLabel);

		return descriptiveSummary;

	}

}
