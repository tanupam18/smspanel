package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.AccountingResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.DescriptiveSummaryResponse;

@Service
public class AccountingService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public AccountingResponse accountingService(String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;
		List<AccountingResponse> list = null;

		Sql = "Select username,DNDType,CreditType  from Main.Customers where username='" + username + "'";

		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			System.out.println(e);
		}
		list = new ArrayList<AccountingResponse>();
		AccountingResponse accountingResponse = null;
		String DndType = null;
		String CreditType = null;

		try {

			while (resultSet.next()) {
				accountingResponse = new AccountingResponse();

				accountingResponse.setCustomerId(resultSet.getString("username"));
				if (resultSet.getInt("DNDType") == 1) {
					DndType = "trans";
				} else if (resultSet.getInt("DNDType") == 2) {
					DndType = "promo";
				} else if (resultSet.getInt("DNDType") == 3) {
					DndType = "open trans";

				} else if (resultSet.getInt("DNDType") == 0) {
					DndType = "scrub";
				} else {
					DndType = "open promo";
				}
				accountingResponse.setAccountType(DndType);
				if (resultSet.getInt("CreditType") == 0) {
					CreditType = "Prepaid";
				} else {
					CreditType = "Postpaid";
				}
				accountingResponse.setMessageType(CreditType);

			}
			resultSet.close();
			connection.close();

		} catch (Exception e) {

		}
		return accountingResponse;

	}
}
