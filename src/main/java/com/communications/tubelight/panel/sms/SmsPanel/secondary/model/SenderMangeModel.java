package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class SenderMangeModel {

	private int page;
	private int size;
	private String search;
	private String sender;
	private boolean delete;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public SenderMangeModel(int page, int size, String search, String sender, boolean delete) {
		super();
		this.page = page;
		this.size = size;
		this.search = search;
		this.sender = sender;
		this.delete = delete;
	}

	public SenderMangeModel() {
		super();
	}

	@Override
	public String toString() {
		return "SenderMangeModel [page=" + page + ", size=" + size + ", search=" + search + ", sender=" + sender
				+ ", delete=" + delete + "]";
	}

}
