package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class schedulePopupModel {

	private int jobId;

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public schedulePopupModel(int jobId) {
		super();
		this.jobId = jobId;
	}

	public schedulePopupModel() {
		super();
	}

	@Override
	public String toString() {
		return "schedulePopupModel [jobId=" + jobId + "]";
	}

}
