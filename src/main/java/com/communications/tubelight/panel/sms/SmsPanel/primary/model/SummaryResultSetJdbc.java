package com.communications.tubelight.panel.sms.SmsPanel.primary.model;

public class SummaryResultSetJdbc {

	private String SenderName;
	private String DlrStatus;
	private String CountDlrStatus;

	public String getSenderName() {
		return SenderName;
	}

	public void setSenderName(String senderName) {
		SenderName = senderName;
	}

	public String getDlrStatus() {
		return DlrStatus;
	}

	public void setDlrStatus(String dlrStatus) {
		DlrStatus = dlrStatus;
	}

	public String getCountDlrStatus() {
		return CountDlrStatus;
	}

	public void setCountDlrStatus(String countDlrStatus) {
		CountDlrStatus = countDlrStatus;
	}

	public SummaryResultSetJdbc(String senderName, String dlrStatus, String countDlrStatus) {
		super();
		SenderName = senderName;
		DlrStatus = dlrStatus;
		CountDlrStatus = countDlrStatus;
	}

	public SummaryResultSetJdbc() {
		super();
	}

	public SummaryResultSetJdbc(String dlrStatus2, String countDlrStatus2) {
	}

	@Override
	public String toString() {
		return "SummaryResultSetJdbc [SenderName=" + SenderName + ", DlrStatus=" + DlrStatus + ", CountDlrStatus="
				+ CountDlrStatus + "]";
	}

}
