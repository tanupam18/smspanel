package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.BulkJobResponse;

@Service
public class BulkJobDownloadService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public List<BulkJobResponse> bulkJob(String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet;

		List<BulkJobResponse> list = null;

		String Sql;

		try {
			Sql = "Select jobid,jobstatus, CampaignName, MessageType, message, MessageLength, sender, TotalNumbers, QueuedAt, CompletedAt, TotalSent from UserJobs Where username='"
					+ username + "' AND  ScheduledAt ='0000-00-00 00:00:00'";

			resultSet = statement.executeQuery(Sql);

			list = new ArrayList<BulkJobResponse>();
			String status = null;

			while (resultSet.next()) {
				BulkJobResponse bulkJobResponse = new BulkJobResponse();

				bulkJobResponse.setSummary(resultSet.getLong("jobid"));
				bulkJobResponse.setFileName(resultSet.getString("CampaignName"));
				bulkJobResponse.setType(resultSet.getString("MessageType"));
				bulkJobResponse.setMessage(URLDecoder.decode(resultSet.getString("Message")));
				bulkJobResponse.setLength(resultSet.getInt("MessageLength"));
				bulkJobResponse.setSender(resultSet.getString("sender"));
				bulkJobResponse.setTcount(resultSet.getInt("TotalNumbers"));
				bulkJobResponse.setQueuedAt(resultSet.getString("QueuedAt"));
				bulkJobResponse.setCompletedAt(resultSet.getString("CompletedAt"));
				bulkJobResponse.setTsent(resultSet.getInt("TotalSent"));
				bulkJobResponse.setStatus(resultSet.getInt("jobstatus"));

				if (resultSet.getInt("jobstatus") == 0) {
					status = "Queued";
				} else if (resultSet.getInt("jobstatus") == 1) {
					status = "Processing";
				} else if (resultSet.getInt("jobstatus") == 2) {
					status = "Partially Processed";
				} else if (resultSet.getInt("jobstatus") == 3) {
					status = "Completed";
				} else {
					status = "Deleted";
				}

				bulkJobResponse.setStatusType(status);

				list.add(bulkJobResponse);
			}
			resultSet.close();

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;

	}
	
	

}
