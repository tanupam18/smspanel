package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class SendTemplateModel {

	private String TemplateName;
	private String DltTemplateId;
	private String sender;
	private String MessageType;
	private String Message;

	public String getTemplateName() {
		return TemplateName;
	}

	public void setTemplateName(String templateName) {
		TemplateName = templateName;
	}

	public String getDltTemplateId() {
		return DltTemplateId;
	}

	public void setDltTemplateId(String dltTemplateId) {
		DltTemplateId = dltTemplateId;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getMessageType() {
		return MessageType;
	}

	public void setMessageType(String messageType) {
		MessageType = messageType;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public SendTemplateModel(String templateName, String dltTemplateId, String sender, String messageType,
			String message) {
		super();
		TemplateName = templateName;
		DltTemplateId = dltTemplateId;
		this.sender = sender;
		MessageType = messageType;
		Message = message;
	}

	@Override
	public String toString() {
		return "SendTemplateModel [TemplateName=" + TemplateName + ", DltTemplateId=" + DltTemplateId + ", sender="
				+ sender + ", MessageType=" + MessageType + ", Message=" + Message + "]";
	}

}
