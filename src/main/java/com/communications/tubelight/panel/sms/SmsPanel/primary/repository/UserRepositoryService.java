package com.communications.tubelight.panel.sms.SmsPanel.primary.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.CustomerModel;

@Service
public class UserRepositoryService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public CustomerModel findByUsername(String username) throws SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection = null;

		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String Sql;
		ResultSet resultSet = null;

		String sql;

		Sql = "Select name, UserId,username,userkey,apikey from Customers where username='" + username + "'";

		resultSet = statement.executeQuery(Sql);
		CustomerModel customerModel = null;
		while (resultSet.next()) {
			customerModel = new CustomerModel();

			customerModel.setId(resultSet.getInt("UserId"));
			customerModel.setUsername(resultSet.getString("username"));
			customerModel.setPassword(resultSet.getString("userkey"));
			customerModel.setName(resultSet.getString("name"));
			customerModel.setApiKey(resultSet.getString("apikey"));

		}
		// if (ip != null && !ip.isEmpty()) {
//			String insertLogin = "INSERT INTO `Main`.`user_login_history` (`user_id`, `user_name`, `ip_address`, `createdOn`) "
//					+ "VALUES ('0', '" + username + "', '" + ip + "', '" + createdAt + "')";
//
//			try {
//				statement.executeUpdate(insertLogin);
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}

		resultSet.close();
		connection.close();
		return customerModel;

	}

	public void loginInsertion(String username, String ip, String createdAt) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection = null;

		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (ip != null && !ip.isEmpty()) {
			String insertLogin = "INSERT INTO `Main`.`user_login_history` (`user_id`, `user_name`, `ip_address`, `createdOn`) "
					+ "VALUES ('0', '" + username + "', '" + ip + "', '" + createdAt + "')";

			try {
				statement.executeUpdate(insertLogin);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
