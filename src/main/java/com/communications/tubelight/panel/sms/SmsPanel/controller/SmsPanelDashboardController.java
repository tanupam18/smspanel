package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.SummaryResultSetJdbc;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.entity.SummaryEntity;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.AllSummaryResultSetJdbc;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.CountSmsRequest;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.CountSmsResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.BalanceService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.SummaryService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
public class SmsPanelDashboardController {

	@Autowired
	SummaryService summaryService;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	BalanceService balanceService;

//	@PostMapping("/count/sms")
//	public Map<String, Object> CountSms(@RequestBody CountSmsRequest countSmsRequest,
//			@RequestHeader Map<String, String> headers) throws ClassNotFoundException, SQLException {
//
////		java.util.regex.Pattern DateValidaton = java.util.regex.Pattern
////				.compile("^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$");
////
////		Matcher CheckingDateValidaton = null;
//
////		try {
////
////			CheckingDateValidaton = DateValidaton.matcher(countSmsRequest.getDate());
////
////		} catch (Exception e) {
////
////		}
////		try {
////			if (CheckingDateValidaton.find() == false) {
////				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
////						"Please Enter the correct date format eg-2021-12-31");
////			}
////
////		} catch (Exception e) {
////
////		}
//
//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
//
//		LocalDate date = Instant.ofEpochMilli(countSmsRequest.getFromDate()).atZone(ZoneId.systemDefault()).toLocalDate();
//		date.toString();
//
//		ArrayList<SummaryResultSetJdbc> summaryEntity = summaryService.countSmsService(countSmsRequest.getSender(),
//				Username, date.toString());
//
////		if (summaryEntity.isEmpty()) {
////			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please provide the corrects details");
////		}
//
//		CountSmsResponse CountStatusDlrStatus = null;
//		CountSmsResponse InfoUsernameSender = null;
//
//		ArrayList<CountSmsResponse> StoringDlr = new ArrayList<>();
//
//		for (int i = 0; i < summaryEntity.size(); i++) {
//
//			CountStatusDlrStatus = new CountSmsResponse();
//			CountStatusDlrStatus.setDlrStatus(summaryEntity.get(i).getDlrStatus());
//			CountStatusDlrStatus.setCountDlrStatus(summaryEntity.get(i).getCountDlrStatus());
//
//			StoringDlr.add(CountStatusDlrStatus);
//		}
//		Map<String, Object> StoringCountSms = new HashMap<String, Object>();
//		StoringCountSms.put("username", Username);
//		StoringCountSms.put("sender", countSmsRequest.getSender());
//		InfoUsernameSender = new CountSmsResponse();
//		InfoUsernameSender.setUsername(Username);
//
//		StoringCountSms.put("dlrStatus", StoringDlr);
//
//		return StoringCountSms;
//
//	}

	@PostMapping("/count/sms/all/sender")
	public Map<String, Object> CountSmsAllSender(@RequestBody CountSmsRequest countSmsRequest,
			@RequestHeader Map<String, String> headers) throws ClassNotFoundException, SQLException {
		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

//		try {
//
//			java.util.regex.Pattern DateValidaton = java.util.regex.Pattern
//					.compile("^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$");
//			Matcher CheckingDateValidaton = DateValidaton.matcher(countSmsRequest.getDate());
//
//			if (CheckingDateValidaton.find() == false) {
//				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
//						"Please Enter the correct date format eg-2021-12-31");
//			}
//
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		ArrayList<AllSummaryResultSetJdbc> allSummaryEntity = summaryService.countAllSmsService(Username,
//				countSmsRequest.getDate());

		ArrayList<CountSmsResponse> StoringDlr = new ArrayList<>();
		CountSmsResponse CountStatusDlrStatus = null;

//		for (int i = 0; i < allSummaryEntity.size(); i++) {
//			CountStatusDlrStatus = new CountSmsResponse();
//
//			CountStatusDlrStatus.setCountDlrStatus(allSummaryEntity.get(i).getCountDlrStatus());
//			CountStatusDlrStatus.setDlrStatus(allSummaryEntity.get(i).getDlrStatus());
//
//			StoringDlr.add(CountStatusDlrStatus);
//		}

		Map<String, Object> StoringCountSms = new HashMap<String, Object>();

		StoringCountSms.put("username", Username);
		StoringCountSms.put("dlrStatus", StoringDlr);

		return StoringCountSms;

	}

	@PostMapping("/dashboard/sms")
	public Map<String, Object> CountSmsAllSenders(@RequestBody CountSmsRequest countSmsRequest,
			@RequestHeader Map<String, String> headers) throws ClassNotFoundException, SQLException {

		Date fromDate = new Date(countSmsRequest.getFromDate());
		DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");

		String fromFormatted = fromFormat.format(fromDate);

		System.out.println("------------------formatted       " + fromFormatted);

		Date toDate = new Date(countSmsRequest.getToDate());
		DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");

		String toFormatted = toFormat.format(toDate);

		System.out.println("------------------formatted       " + toFormatted);

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		Map<String, Object> StoringCountSms = null;
		Map<String, Object> DlrStatus = null;
		Map<String, Object> dashBoardAll = null;

		if (countSmsRequest.getSender() != null && !countSmsRequest.getSender().isEmpty()) {

			ArrayList<SummaryResultSetJdbc> summaryEntity = summaryService.countSmsService(countSmsRequest.getSender(),
					Username, fromFormatted, toFormatted);
			CountSmsResponse CountStatusDlrStatus = null;
			CountSmsResponse InfoUsernameSender = null;

			ArrayList<CountSmsResponse> StoringDlr = new ArrayList<>();

			int submissionSms = 0;
			for (int i = 0; i < summaryEntity.size(); i++) {
				submissionSms += Integer.parseInt(summaryEntity.get(i).getCountDlrStatus());
			}
			Map<String, Object> storingAllCountSms = new HashMap<>();

			for (int i = 0; i < summaryEntity.size(); i++) {

				DlrStatus = new HashMap<String, Object>();
				CountStatusDlrStatus = new CountSmsResponse();

				CountStatusDlrStatus.setDlrStatus(summaryEntity.get(i).getDlrStatus());
				CountStatusDlrStatus.setCountDlrStatus(summaryEntity.get(i).getCountDlrStatus());
				DlrStatus.put(summaryEntity.get(i).getDlrStatus(), summaryEntity.get(i).getCountDlrStatus());

				storingAllCountSms.putAll(DlrStatus);

				StoringDlr.add(CountStatusDlrStatus);
			}
			dashBoardAll = new HashMap<String, Object>();

			StoringCountSms = new HashMap<String, Object>();
			StoringCountSms.put("username", Username);
			// StoringCountSms.put("sender", countSmsRequest.getSender());
			StoringCountSms.put("submission_sms", submissionSms);

			InfoUsernameSender = new CountSmsResponse();
			InfoUsernameSender.setUsername(Username);

			StoringCountSms.put("dlrStatus", storingAllCountSms);
			// dashBoardAll.put("All", StoringCountSms);
		} else {
			ArrayList<AllSummaryResultSetJdbc> allSummaryEntity = summaryService.countAllSmsService(Username,
					fromFormatted, toFormatted);

			int submissionSms = 0;
			for (int i = 0; i < allSummaryEntity.size(); i++) {

				System.out.println("AllSummaryENtity----------" + allSummaryEntity.get(i).getCountDlrStatus());

				submissionSms += Integer.parseInt(allSummaryEntity.get(i).getCountDlrStatus());
			}
			System.out.println("-------------sum-----------------------------" + submissionSms);

			ArrayList<CountSmsResponse> StoringDlr = new ArrayList<>();
			CountSmsResponse CountStatusDlrStatus = null;

			StoringCountSms = new HashMap<String, Object>();
			Map<String, Object> storingAllCountSms = new HashMap<>();

			for (int i = 0; i < allSummaryEntity.size(); i++) {
				DlrStatus = new HashMap<String, Object>();

				CountStatusDlrStatus = new CountSmsResponse();

				CountStatusDlrStatus.setCountDlrStatus(allSummaryEntity.get(i).getCountDlrStatus());
				CountStatusDlrStatus.setDlrStatus(allSummaryEntity.get(i).getDlrStatus());

				DlrStatus.put(allSummaryEntity.get(i).getDlrStatus(), allSummaryEntity.get(i).getCountDlrStatus());
				System.out.println("DLrStatus-------------------------------------" + DlrStatus);
				storingAllCountSms.putAll(DlrStatus);

			}
			System.out.println("DLrStatusOutsideOfForLoop-------------------------------------" + DlrStatus);

			dashBoardAll = new HashMap<String, Object>();

			StoringCountSms.put("submission_sms", submissionSms);
			StoringCountSms.put("username", Username);
			StoringCountSms.put("dlrStatus", storingAllCountSms);

			// dashBoardAll.put("All", StoringCountSms);

		}

		return StoringCountSms;

	}

	@PostMapping("/balance")
	public Map<String, String> getMoney(@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);
		Map<String, String> AllBalance = new HashMap<String, String>();
		String balance = null;

		try {
			balance = balanceService.balanceService(Username);
			AllBalance.put("balance", balance);

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println(e);
		}
		return AllBalance;

	}

}
