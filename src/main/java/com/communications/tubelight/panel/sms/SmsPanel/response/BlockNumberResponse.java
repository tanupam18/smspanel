package com.communications.tubelight.panel.sms.SmsPanel.response;

public class BlockNumberResponse {

	private String username;
	private String Mobile;
	private String AddedIt;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getAddedIt() {
		return AddedIt;
	}

	public void setAddedIt(String addedIt) {
		AddedIt = addedIt;
	}

	public BlockNumberResponse(String username, String mobile, String addedIt) {
		super();
		this.username = username;
		Mobile = mobile;
		AddedIt = addedIt;
	}

	public BlockNumberResponse() {
		super();
	}

	@Override
	public String toString() {
		return "BlockNumberResponse [username=" + username + ", Mobile=" + Mobile + ", AddedIt=" + AddedIt + "]";
	}

}
