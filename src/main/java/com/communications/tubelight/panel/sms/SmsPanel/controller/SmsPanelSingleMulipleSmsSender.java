package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.DeleteTemplateModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.MatchTemplateModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.MultipleSmsSource;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.RetrieveTemplateModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SendTemplateModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SingleSmsProcedure;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.TemplateRetrievingRequest;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MatchTemplateService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MulipleSendSmsService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.PersonalizeService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.TemplateDeleteService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.TemplateRetrieveService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.TemplateSendService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.UploadFIleService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.communications.tubelight.panel.sms.SmsPanel.Interface.FilesStorageService;
import com.communications.tubelight.panel.sms.SmsPanel.primary.model.CustomerModel;
import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.UserRepositoryService;
import com.communications.tubelight.panel.sms.SmsPanel.primary.service.AllSenderService;
import com.communications.tubelight.panel.sms.SmsPanel.primary.service.SmsCountService;
import com.communications.tubelight.panel.sms.SmsPanel.primary.service.SmsLengthCalculatorService;
import com.communications.tubelight.panel.sms.SmsPanel.response.AllsenderResponse;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
public class SmsPanelSingleMulipleSmsSender {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	SmsCountService smsCountService;

	@Autowired
	SmsLengthCalculatorService smsLengthCalculatorService;

	@Autowired
	MulipleSendSmsService mulipleSendSmsService;

	@Autowired
	TemplateSendService templateSendService;

	@Autowired
	TemplateRetrieveService templateRetrieveService;

	@Autowired
	TemplateDeleteService templateDeleteService;

	@Autowired
	FilesStorageService storageService;

	@Autowired
	UploadFIleService uploadFIleService;

	@Autowired
	MatchTemplateService matchTemplateService;

	@Autowired
	AllSenderService allSenderService;

	@Autowired
	UserRepositoryService repositoryService;

	@Autowired
	UserRepositoryService userRepositoryService;

	@Autowired
	PersonalizeService personalizeServices;

	@PostMapping("/websms/single")
	public Map<String, Object> blawla(@RequestBody SingleSmsProcedure SingleSmsProcedure,
			@RequestHeader Map<String, String> headers) throws IOException, SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);
		System.out.println("Username-------------------------------" + Username);
		int smsLength = 0;

		String pattern = "^(91)([6789]{1}\\d{9})$";
		if (SingleSmsProcedure.getMobileNo().matches(pattern)) {
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please enter correct Mobile number");

		}
		int msgType = 0;

		if (SingleSmsProcedure.getMessageType().toUpperCase().equals("TEXT")) {
			msgType = 0;

		} else if (SingleSmsProcedure.getMessageType().toUpperCase().equals("UNICODE")) {
			msgType = 1;
		} else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Message type Must be Text or Unicode");
		}
		CustomerModel user = null;
		user = repositoryService.findByUsername(Username);
		System.out.println("---------User---Password--------" + user.getPassword());
		int countSms = smsCountService.getSmsCount(msgType, smsLength);

		smsLength = smsLengthCalculatorService
				.getSmsLength(URLDecoder.decode(SingleSmsProcedure.getMessages(), "UTF-8"));

		OkHttpClient client = new OkHttpClient();

		String uuid = UUID.randomUUID().toString();

		HttpUrl.Builder urlBuilder = HttpUrl.parse("http://10.0.8.80:8080/SmsHandler/APIRequest").newBuilder();

		urlBuilder.addQueryParameter("SmsId", uuid);
		urlBuilder.addQueryParameter("Username", Username);
		if (user.getApiKey().equals("None")) {
			urlBuilder.addQueryParameter("Password", URLDecoder.decode(user.getPassword(), "UTF-8"));

		} else {
			urlBuilder.addQueryParameter("Password", URLDecoder.decode(user.getApiKey(), "UTF-8"));
		}
		urlBuilder.addQueryParameter("Destination", SingleSmsProcedure.getMobileNo());
		urlBuilder.addQueryParameter("Sender", SingleSmsProcedure.getSender());
		urlBuilder.addQueryParameter("SmsCount", String.valueOf(countSms));
		urlBuilder.addQueryParameter("DataCoding", "0");
		urlBuilder.addQueryParameter("Message", URLEncoder.encode(SingleSmsProcedure.getMessages(), "UTF-8"));
		urlBuilder.addQueryParameter("EsmClass", "1");
		urlBuilder.addQueryParameter("SmsLength", String.valueOf(smsLength));
		urlBuilder.addQueryParameter("dndType", "0");
		urlBuilder.addQueryParameter("clientIp", "localhost");

		String url = URLDecoder.decode(urlBuilder.build().toString(), "UTF-8");
		// String url = urlBuilder.build().toString();

		URL url1 = new URL(url);
		String tmid = null;
		String providerId = null;

		BufferedReader in = new BufferedReader(new InputStreamReader(url1.openStream()));
		String word;
		while ((word = in.readLine()) != null) {
			String[] parts = word.split("-");
			System.out.println(parts[18].toString().substring(2, parts[18].toString().length()));
			providerId = parts[4].toString().substring(2, parts[4].toString().length() - 2);
			tmid = parts[18].toString().substring(2, parts[18].toString().length());
			System.out.println("parts check----------" + parts[1]);
			System.out.println("result = " + Arrays.toString(parts));
		}
		in.close();

		Request request = new Request.Builder().url(url).build();

		System.out.println("UrlBuilder-----------------" + url);

		Response response = client.newCall(request).execute();

		response.body().close();
		// String Batch_Id1 = rootNode.path("protocol").toString();

//		System.out.println("BatchId---------------"+Batch_Id);
		// System.out.println("BatchId1---------------"+Batch_Id1);

		int MessageClass = 0;
		int MessageCoding = 0;
		if (msgType == 0) {
			MessageClass = 1;
			MessageCoding = 0;
		} else if (msgType == 1) {
			MessageClass = -1;
			MessageCoding = 2;
		}

//		String MetaData = "?smpp?TELEMARKETER_ID=" + tmid
//				+ "&PE_ID=1001592264572351618&TEMPLATE_ID=1007414827014187717";
		String MetaData = "?smpp?TELEMARKETER_ID=" + tmid + "&PE_ID=" + SingleSmsProcedure.getPeId() + "&TEMPLATE_ID="
				+ SingleSmsProcedure.getTempId();

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("tube_Logs.spInsertSingleSmsFinal");

		query.registerStoredProcedureParameter("sMsgId", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("iJobId", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("MOMT", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sSender", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sMobile", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sMessage", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sProviderId", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sUsername", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("iSmsType", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("iMessageClass", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("iMessageCoding", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("iCreditsToDeduct", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("iSmsLength", Integer.class, ParameterMode.IN);

		query.registerStoredProcedureParameter("sAltSender", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sMetaData", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sStatus", String.class, ParameterMode.IN);

		query.registerStoredProcedureParameter("sError", String.class, ParameterMode.IN);

		query.setParameter("sMsgId", uuid);//
		query.setParameter("iJobId", 0);
		query.setParameter("MOMT", "MT");
		query.setParameter("sSender", SingleSmsProcedure.getSender());
		query.setParameter("sMobile", SingleSmsProcedure.getMobileNo());
		query.setParameter("sMessage", URLEncoder.encode(SingleSmsProcedure.getMessages(), "UTF-8"));
		query.setParameter("sProviderId", providerId);
		query.setParameter("sUsername", Username);
		query.setParameter("iSmsType", msgType);
		query.setParameter("iMessageClass", MessageClass);
		query.setParameter("iMessageCoding", MessageCoding);
		query.setParameter("iCreditsToDeduct", countSms);
		query.setParameter("iSmsLength", smsLength);
		query.setParameter("sAltSender", "reseller");
		query.setParameter("sMetaData", MetaData);
		query.setParameter("sStatus", "PENDING");
		query.setParameter("sError", "205");

		HashMap<String, Object> map = new HashMap<>();

		System.out.println("sMsgId--" + "0   " + "iJobId----" + 0 + "-----MOMT" + "MT  " + "sSender-----"
				+ SingleSmsProcedure.getSender() + "   sMobile----" + SingleSmsProcedure.getMobileNo()
				+ "------  sMessage----" + SingleSmsProcedure.getMessages() + "    sProviderId " + providerId
				+ "----------" + " sUsername----" + Username + "------iSmsType " + msgType + "----------"
				+ "iMessageClass-------" + MessageClass + "------" + "-------iMessageCoding-" + MessageCoding
				+ "-------iCreditsToDeduct" + countSms + "---------iSmsLength" + smsLength
				+ "-----------sAltSender------------" + "reseller" + "---------sMetaData-----" + MetaData
				+ "-----------" + "sStatus--------" + "PENDING" + "--------sError-----" + "205");
		// try{

		query.execute();

		map.put("message", "Message Sent Successfully");

		// } catch (Exception e) {
		// throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Contact to Support
		// team");

		// }

		return map;

	}

	@PostMapping("/websms/multiple")
	public Map<String, Object> CountSmsdatewiseSender(@RequestHeader Map<String, String> headers,
			@RequestBody MultipleSmsSource multipleSmsSource)
			throws ClassNotFoundException, SQLException, UnsupportedEncodingException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		System.out.println("FILENAME----------------------" + multipleSmsSource.getFileName());

		HashMap<String, Object> map = new HashMap<>();

		int msgType = 0;

		if (multipleSmsSource.getSmsType().toUpperCase().equals("TEXT")) {
			msgType = 0;

		} else if (multipleSmsSource.getSmsType().toUpperCase().equals("UNICODE")) {
			msgType = 1;
		} else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Message type Must be Text or Unicode");
		}
		DateFormat format = null;
		String formatted = null;
		if (multipleSmsSource.getFromDate() != null) {
			Date date = new Date(multipleSmsSource.getFromDate());
			format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatted = format.format(date);
			System.out.println("---------------formatted" + formatted);

		} else {
			formatted = "0000-00-00 00:00:00";
		}

		int smsLength = smsLengthCalculatorService
				.getSmsLength(URLDecoder.decode(multipleSmsSource.getMessage(), "UTF-8"));

		int countSms = smsCountService.getSmsCount(msgType, smsLength);

		String metaData = "?smpp?TELEMARKETER_ID=&PE_ID=" + multipleSmsSource.getPeId() + "&TEMPLATE_ID="
				+ multipleSmsSource.getTemplateId();

		try {
			mulipleSendSmsService.sendMultipleData(multipleSmsSource, Username, formatted, smsLength, countSms,
					metaData);
			map.put("message", "Message uploaded Successfully");
			// return map;
			return map;

		} catch (Exception e) {
			System.out.println(e);
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Please Contact to support team");
		}

	}

	@PostMapping("/websms/template")
	public HashMap<String, Object> SendingTemplate(@RequestHeader Map<String, String> headers,
			@RequestBody SendTemplateModel sendTemplateModel)
			throws ClassNotFoundException, SQLException, UnsupportedEncodingException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		int msgType = 0;

		int a = templateSendService.sendTemplateService(sendTemplateModel, Username);
		System.out.println("-------------------------------------" + a);
		if (a > 0) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Template already exist");
		} else {
			// throw new ResponseStatusException(HttpStatus.OK, "Template added
			// successfully");
			HashMap<String, Object> template = new HashMap<String, Object>();
			template.put("message", "Template added successsfully");
			return template;
		}

	}

	@PostMapping("/websms/personalize")
	public Map<String, Object> SendPersonalizeMessage(@RequestHeader Map<String, String> headers,
			@RequestBody MultipleSmsSource multipleSmsSource)
			throws ClassNotFoundException, SQLException, UnsupportedEncodingException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		int msgType = 0;

		if (multipleSmsSource.getSmsType().toUpperCase().equals("TEXT")) {
			msgType = 0;

		} else if (multipleSmsSource.getSmsType().toUpperCase().equals("UNICODE")) {
			msgType = 1;
		} else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Message type Must be Text or Unicode");
		}

		int smsLength = smsLengthCalculatorService
				.getSmsLength(URLDecoder.decode(multipleSmsSource.getMessage(), "UTF-8"));

		DateFormat format = null;
		String formatted = null;
		if (multipleSmsSource.getFromDate() != null && !multipleSmsSource.getFromDate().toString().isEmpty()) {
			Date date = new Date(multipleSmsSource.getFromDate());
			format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
			formatted = format.format(date);

		}
		HashMap<String, Object> map = new HashMap<>();

		System.out.println("-----formatted----" + formatted);

		String metaData = "?smpp?TELEMARKETER_ID=&PE_ID=" + multipleSmsSource.getPeId() + "&TEMPLATE_ID="
				+ multipleSmsSource.getTemplateId();
		try {
			System.out.println("MMMMMMMmetadata" + metaData);

			personalizeServices.personalizeService(Username, multipleSmsSource.getMessage(), msgType, smsLength,
					multipleSmsSource.getSender(), formatted, multipleSmsSource.getFileName(),
					multipleSmsSource.getTotalNumber(), metaData);

			map.put("message", "Message Sent Successfully");

		} catch (Exception e) {
			System.out.println(e.getStackTrace()[0].getLineNumber());
		}

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("spAddUserJob");
		query.registerStoredProcedureParameter("sUsername", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sMessage", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sMessageType", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sMessageLength", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("sSender", String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("iJobType", Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter("dtScheduled", String.class, ParameterMode.IN);

		query.setParameter("iJobType", 0);
		query.setParameter("sUsername", Username);
		query.setParameter("sMessage", multipleSmsSource.getMessage());
		query.setParameter("sMessageType", msgType);
		query.setParameter("sMessageLength", smsLength);
		query.setParameter("sSender", multipleSmsSource.getSender());
		if (multipleSmsSource.getFromDate() != null) {
			query.setParameter("dtScheduled", formatted);

		} else {
			// query.setParameter("dtScheduled", "1970-01-10 00:00:00");
			query.setParameter("dtScheduled", "0000-00-00 00:00:00");
		}

		try {
			// query.execute();
			// String sql = "SELECT LAST_INSERT_ID()";

		} catch (Exception e) {
			System.out.println(e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Contact to Suppor team");

		}

		return map;

	}

	@PostMapping("/websms/retrieve/template")
	public Map<String, Object> retrieveTemplate(@RequestHeader Map<String, String> headers,
			@RequestBody TemplateRetrievingRequest templateRetrievingRequest) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		RetrieveTemplateModel retrieveTemplateModel = null;
		ArrayList<RetrieveTemplateModel> a = templateRetrieveService.retrieveTemplate(Username,
				templateRetrievingRequest.getSearch());
		ArrayList<RetrieveTemplateModel> StoringDlr = new ArrayList<>();

		for (int i = 0; i < a.size(); i++) {
			retrieveTemplateModel = new RetrieveTemplateModel();

			retrieveTemplateModel.setTemplateMessage(a.get(i).getTemplateMessage());
			retrieveTemplateModel.setTemplateName(a.get(i).getTemplateName());
			retrieveTemplateModel.setSender(a.get(i).getSender());
			retrieveTemplateModel.setTemplateDltId(a.get(i).getTemplateDltId());
			retrieveTemplateModel.setSmsType(a.get(i).getSmsType());
			StoringDlr.add(retrieveTemplateModel);

		}

		Map<String, Object> StoringCountSms = new HashMap<String, Object>();

		StoringCountSms.put("username", Username);
		StoringCountSms.put("template_info", StoringDlr);

		return StoringCountSms;

	}

	@DeleteMapping("/websms/retrieve/template/delete")
	public Object deleteTemplate(@RequestHeader Map<String, String> headers,
			@RequestBody DeleteTemplateModel sendTemplateModel) throws ClassNotFoundException, SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		templateDeleteService.TemplateDltService(sendTemplateModel.getTemplateId(), sendTemplateModel.getSender(),
				Username);
		Map<String, Object> templatedelete = new HashMap<String, Object>();

		templatedelete.put("message", "Template Deleted Successfully");

		return templatedelete;

	}

	@PostMapping("/upload")
	public Map<String, Object> uploadFile(@RequestHeader Map<String, String> headers,
			@RequestParam("file") MultipartFile file) {
		String message = "";
		String fileName = "";
		// try {
		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		int jobId = mulipleSendSmsService.getJobId();

		Map<String, Object> AllRecordDetails = storageService.save(file, Username);

		message = "Uploaded the file successfully: " + file.getOriginalFilename();
		fileName = storageService.UploadFileName();

		// return ResponseEntity.status(HttpStatus.OK).body(new
		// UploadResponseMessage(message,fileName));
		return AllRecordDetails;
		// } catch (Exception e) {
		// System.out.println(e);
		// message = "Could not upload the file: " + file.getOriginalFilename() + "!";
		// return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new
		// UploadResponseMessage(message));
		// throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Upload file must be
		// csv");
		// }
	}

	@PostMapping("/personalize//upload")
	public Map<String, Object> personalizeUploadFile(@RequestHeader Map<String, String> headers,
			@RequestParam("file") MultipartFile file) {
		String message = "";
		String fileName = "";
		// try {
		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		Map<String, Object> AllRecordDetails = storageService.personalizeSave(file, Username);

		message = "Uploaded the file successfully: " + file.getOriginalFilename();

		return AllRecordDetails;
		// } catch (Exception e) {
		// System.out.println(e);
		// message = "Could not upload the file: " + file.getOriginalFilename() + "!";
		// throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Upload file must be
		// csv");
		// }
	}

	@PostMapping("/match/template")
	public Map<String, Object> matchTemplate(@RequestHeader Map<String, String> headers,
			@RequestBody MatchTemplateModel matchTemplateModel) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		String a = null;
		try {
			a = matchTemplateService.matchTemplate(matchTemplateModel.getMessage(), Username,
					matchTemplateModel.getSender());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("a-----------------------------" + a);

		Map<String, Object> template = new HashMap<String, Object>();

		if (a != null) {
			template.put("message", "Template Matched");

		} else {
			template.put("message", "Template not Matched");
		}
		return template;
	}

	@PostMapping("/sender")
	public ArrayList<Object> AllSender(@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		ArrayList<AllsenderResponse> a = allSenderService.allSender(Username);
		AllsenderResponse allsenderResponse = null;
		Map<String, Object> allSenderStore = null;
		Map<String, Object> allSenderStore2 = new HashMap<String, Object>();
		ArrayList<Object> arrayList = new ArrayList<Object>();
		for (int i = 0; i < a.size(); i++) {
			allSenderStore = new HashMap<String, Object>();
			allsenderResponse = new AllsenderResponse();
			// allsenderResponse.setSender(a.get(i).getSender());
			arrayList.add(a.get(i).getSender());
		}

		return arrayList;

	}

	@PostMapping("/smstype")
	public ArrayList<Object> AllSmsType(@RequestHeader Map<String, String> headers) {

		ArrayList<Object> arrayList = new ArrayList<Object>();
		arrayList.add("TEXT");
		arrayList.add("UNICODE");

		return arrayList;

	}

	@PostMapping("/download/center/column")
	public ArrayList<String> AllColumn() {
		ArrayList<String> AllColumn = new ArrayList<String>();

		AllColumn.add("Message Id");
		AllColumn.add("Message Count");
		AllColumn.add("Username");
		AllColumn.add("Sent Time");
		AllColumn.add("Source");
		AllColumn.add("Delivery Time");
		AllColumn.add("Destination");
		AllColumn.add("Delivery Status");
		AllColumn.add("JobId");
		AllColumn.add("Error Code");
		AllColumn.add("Message Length");
		AllColumn.add("Message");

		return AllColumn;

	}

	@PostMapping("/download/center/status")
	public ArrayList<String> AllStatus() {
		ArrayList<String> AllColumn = new ArrayList<String>();

		AllColumn.add("pending");
		AllColumn.add("queue");
		AllColumn.add("completed");

		return AllColumn;

	}

	@PostMapping("/bulk/template")
	public Map<String, String> bulkTemplateMode(@RequestHeader Map<String, String> headers,
			@RequestParam("file") MultipartFile file) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");
		String Username = jwtUtils.getUserNameFromJwtToken(token);

		Map<String, String> message = null;

		try {
			storageService.bulkUploadStorage(file, Username);
			message = new HashMap<String, String>();
			message.put("message", "Template Added Successfully");
			return message;

		} catch (Exception e) {
			message.put("message", "Please Contact to support team");
			return message;
		}

	}
}