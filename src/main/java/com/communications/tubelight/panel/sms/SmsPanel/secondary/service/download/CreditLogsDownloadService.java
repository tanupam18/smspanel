package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.CreditLogResponse;

@Service
public class CreditLogsDownloadService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	private int noOfRecords;

	public List<CreditLogResponse> creditLogService(String username) throws SQLException {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;
		List<CreditLogResponse> list = null;

		Sql = "SELECT LoggedOn, Username, NewCreditAlloted, OldCreditAlloted, NewBalance, OldBalance FROM CreditLog WHERE Username = '"
				+ username+"'";

		System.out.println(Sql);

		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			System.out.println(e);
		}
		list = new ArrayList<CreditLogResponse>();

		try {

			while (resultSet.next()) {
				CreditLogResponse creditLogResponse = new CreditLogResponse();

				creditLogResponse.setAdjustedAt(resultSet.getString("LoggedOn"));
				creditLogResponse.setAdjustment("30");
				creditLogResponse.setUsername(resultSet.getString("Username"));
				creditLogResponse.setNewBalance(resultSet.getString("NewBalance"));
				creditLogResponse.setOldBalance(resultSet.getString("OldBalance"));
				creditLogResponse.setNewCreditAlloted(resultSet.getString("NewCreditAlloted"));
				creditLogResponse.setOldCreditAlloted(resultSet.getString("OldCreditAlloted"));

				list.add(creditLogResponse);

			}
			resultSet.close();

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			connection.close();
		}
		return list;

	}

}
