package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class AccountingupdateModel {
	String Password;

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public AccountingupdateModel(String password) {
		super();
		Password = password;
	}

	public AccountingupdateModel() {
		super();
	}

	@Override
	public String toString() {
		return "AccountingupdateModel [Password=" + Password + "]";
	}

}
