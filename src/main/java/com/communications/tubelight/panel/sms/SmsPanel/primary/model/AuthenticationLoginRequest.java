package com.communications.tubelight.panel.sms.SmsPanel.primary.model;

public class AuthenticationLoginRequest {
	private String ip;
	private String createdAt;
	private String username;
	private String password;
	// private Integer expirydate;

	public AuthenticationLoginRequest(String username, String password) {
		this.username = username;
		this.password = password;
		// this.expirydate = expirydate;
	}

//	public Integer getExpirydate() {
//		return expirydate;
//	}

//	public void setExpirydate(Integer expirydate) {
//		this.expirydate = expirydate;
//	}

	public String getUsername() {
		return username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AuthenticationLoginRequest(String ip, String createdAt, String username, String password) {
		super();
		this.ip = ip;
		this.createdAt = createdAt;
		this.username = username;
		this.password = password;
	}

	public AuthenticationLoginRequest() {
		super();
	}

	@Override
	public String toString() {
		return "AuthenticationLoginRequest [ip=" + ip + ", createdAt=" + createdAt + ", username=" + username
				+ ", password=" + password + "]";
	}

}