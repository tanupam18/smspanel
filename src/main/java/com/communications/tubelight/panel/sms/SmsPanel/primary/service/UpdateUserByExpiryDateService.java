package com.communications.tubelight.panel.sms.SmsPanel.primary.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.primary.entity.User;
import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.UserRepository;

@Service
public class UpdateUserByExpiryDateService {

	@Autowired
	UserRepository userRepository;

	public Integer updateProductByepiryDate(Long id, Integer expirydate) {
		try {
			User user = userRepository.findById(id).orElse(null);
			user.setExpirydate(expirydate);
			user.setSecretkey(UUID.randomUUID().toString());
			userRepository.save(user);
			return user.getExpirydate();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

}