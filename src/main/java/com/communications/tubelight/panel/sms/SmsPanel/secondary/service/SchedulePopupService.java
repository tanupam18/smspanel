package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.PopupResponse;

@Service
public class SchedulePopupService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public Map<String, Object> popupService(int jobid) throws SQLException {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet;

		List<PopupResponse> list = null;
		String Sql;
		String MessageType;
		Map<String, Object> SchedulePopup = new HashMap<String, Object>();
		try {

			Sql = "Select jobid,jobstatus, CampaignName, MessageType, message, MessageLength, sender, TotalNumbers, QueuedAt, CompletedAt, TotalSent from UserJobs where jobid='"
					+ jobid + "'";
			resultSet = statement.executeQuery(Sql);
			list = new ArrayList<PopupResponse>();
			System.out.println(Sql);

			PopupResponse pp = new PopupResponse();
			ArrayList graphData = new ArrayList();
			while (resultSet.next()) {

				// PopupResponse pp = new PopupResponse();
				pp.setDate(resultSet.getString("CompletedAt"));
				pp.setMessageCount(resultSet.getInt("TotalNumbers")); // TotalNumbers
				pp.setId(resultSet.getInt("jobid"));
				pp.setMask(resultSet.getString("sender"));
				pp.setMessage(resultSet.getString("message"));
				if (resultSet.getInt("MessageType") == 0) {
					MessageType = "TEXT";
				} else {
					MessageType = "UNICODE";
				}
				pp.setMessageType(resultSet.getInt("MessageType"));
			}
			SchedulePopup.put("form-data", pp);
			resultSet.close();

			Sql = " SELECT  DeliveryStatus, COUNT(JobId) AS MsgSent, SUM(MsgCount) as Part  FROM tube_Logs.PendingSms WHERE JobId ='"
					+ jobid + "' GROUP BY DeliveryStatus";
			System.out.println(Sql);
			resultSet = statement.executeQuery(Sql);

			while (resultSet.next()) {
				PopupResponse qq = new PopupResponse();

				qq.setCount(resultSet.getInt("MsgSent"));
				qq.setStatus(resultSet.getString("DeliveryStatus"));
				qq.setParts(resultSet.getInt("Part"));
				graphData.add(qq);
			}
			SchedulePopup.put("graph-data", graphData);

		} catch (Exception e) {
			System.out.println(e);
		}
		connection.close();
		return SchedulePopup;

	}
}
