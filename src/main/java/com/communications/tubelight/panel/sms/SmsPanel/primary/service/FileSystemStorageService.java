package com.communications.tubelight.panel.sms.SmsPanel.primary.service;

import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.configure.FileUploadProperties;
import com.communications.tubelight.panel.sms.SmsPanel.exception.FileNotFoundException;
import com.communications.tubelight.panel.sms.SmsPanel.exception.FileStorageException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileSystemStorageService {
    
	//private final Path dirLocation;
	
	private final Path root = Paths.get("uploads");

   
//	@Autowired
//    public FileSystemStorageService(FileUploadProperties fileUploadProperties) {
//        this.dirLocation = Paths.get(fileUploadProperties.getLocation())
//                                .toAbsolutePath()
//                                .normalize();
//    }
	
//    @PostConstruct
//    public void init() {
//        	 try {
//        	      Files.createDirectory(root);
//        	    } catch (IOException e) {
//        	      throw new RuntimeException("Could not initialize folder for upload!");
//        	    }
//        	  
//        
//       
//    }
    public String saveFile(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            //Path dfile = this.dirLocation.resolve(fileName);
           Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));

            return fileName;
            
        } catch (Exception e) {
            throw new FileStorageException("Could not upload file");
        }
    }

	
	
//    @Override
//    public Resource loadFile(String fileName) {
//        // TODO Auto-generated method stub
//        try {
//          Path file = this.dirLocation.resolve(fileName).normalize();
//          Resource resource = new UrlResource(file.toUri());
//
//          if (resource.exists() || resource.isReadable()) {
//              return resource;
//          } 
//          else {
//              throw new FileNotFoundException("Could not find file");
//          }
//        } 
//        catch (MalformedURLException e) {
//            throw new FileNotFoundException("Could not download file");
//        }           
//    }
}