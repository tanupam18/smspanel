package com.communications.tubelight.panel.sms.SmsPanel.response;

public class LoginRefreshTokenResponse {

	private String refreshToken;

	public String getRefreshtoken() {
		return refreshToken;
	}

	public void setRefreshtoken(String refreshtoken) {
		this.refreshToken = refreshtoken;
	}

	public LoginRefreshTokenResponse(String refreshtoken) {
		this.refreshToken = refreshtoken;
	}
}
