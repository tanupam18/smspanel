package com.communications.tubelight.panel.sms.SmsPanel.primary.model;

public class TokenRefreshRequest {

	private String refreshToken;

	private Integer expirydate = 1;

	public Integer getExpirydate() {
		return expirydate;
	}

	public void setExpirydate(Integer expirydate) {
		this.expirydate = expirydate;
	}

	public TokenRefreshRequest(String refreshToken, Integer expirydate) {
		this.refreshToken = refreshToken;
		this.expirydate = expirydate;
	}

	public TokenRefreshRequest() {

	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
}
