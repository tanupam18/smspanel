package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class PauseRestartModel {
	private int jobId;
	private String update;

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public PauseRestartModel(int jobId, String update) {
		super();
		this.jobId = jobId;
		this.update = update;
	}

	public PauseRestartModel() {
		super();
	}

	@Override
	public String toString() {
		return "PauseRestartModel [jobId=" + jobId + ", update=" + update + "]";
	}

}
