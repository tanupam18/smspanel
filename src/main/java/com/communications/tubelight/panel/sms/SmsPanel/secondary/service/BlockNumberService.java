package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.BlockNumberResponse;

@Service
public class BlockNumberService {

	private int noOfRecords;
	private boolean preventInsertion = false;

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public boolean preventInsertionMultipleBlock() {
		return this.preventInsertion = true;

	}

	public List<BlockNumberResponse> blockService(int offset, int size, String search, String mobile, String username,
			boolean delete) throws SQLException {

		if (delete == true) {
			BlockNumberService blockNumberService = new BlockNumberService();
			blockNumberService.deleteManageSender(mobile, username);

		}

		Connection connection = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String Sql;
		ResultSet resultSet = null;

		if (search != null && !search.isEmpty()) {

		} else {
			if (delete == false && mobile != null && !mobile.isEmpty()) {
				if (this.preventInsertion != true) {
					String InsertSql = "INSERT INTO BlockNumber(`Mobile`, `Username`) VALUES ('" + mobile + "', '"
							+ username + "')";
					System.out.println("InsertSql-------------------------" + InsertSql);
					System.out.println("preventInsertion------------" + this.preventInsertion);
					//
					statement.executeUpdate(InsertSql);
				}

			}
		}
		String ManageSenderSql = null;

		if (search != null && !search.isEmpty()) {
			ManageSenderSql = "Select Mobile, Username, AddedAt, IsDeleted from BlockNumber where username='" + username
					+ "' AND (mobile like '%" + search + "%' OR username like '%" + search + "%') limit " + size;

		} else {
			ManageSenderSql = "Select Mobile, Username, AddedAt, IsDeleted from BlockNumber where username='" + username
					+ "' limit " + offset + ", " + size;

		}
		System.out.println("ManageSenderSql-------------" + ManageSenderSql);

		List<BlockNumberResponse> allblockNumberResponse = null;

		try {
			allblockNumberResponse = new ArrayList<BlockNumberResponse>();

			resultSet = statement.executeQuery(ManageSenderSql);
			while (resultSet.next()) {
				BlockNumberResponse BlockNumberResponse = new BlockNumberResponse();

				BlockNumberResponse.setUsername(resultSet.getString("Username"));
				BlockNumberResponse.setMobile(resultSet.getString("Mobile"));
				BlockNumberResponse.setAddedIt(resultSet.getString("AddedAt"));

				allblockNumberResponse.add(BlockNumberResponse);

			}
			resultSet.close();

			String CountSql = "Select count(*) as count from BlockNumber where username = '" + username + "'";
			System.out.println("---------COUNTSQL-----------" + CountSql);
			resultSet = statement.executeQuery(CountSql);

			if (resultSet.next()) {
				this.noOfRecords = resultSet.getInt("count");
			}

			resultSet.close();
		} catch (SQLException e) {
			System.out.println(e);
			System.out.println(e.getStackTrace()[0].getLineNumber());
		}
		this.preventInsertion = false;

		return allblockNumberResponse;

	}

	public int getRecord() {
		return this.noOfRecords;
	}

	public void deleteManageSender(String mobile, String username) throws SQLException {
		String Local = "jdbc:mysql://localhostss:3306/Mains?useSSL=false&allowPublicKeyRetrieval=true&sessionVariables=sql_mode='NO_ENGINE_SUBSTITUTION'&jdbcCompliantTruncation=false";
		String UsernameConnection = "kannel";
		String Password = "sm@rTy51";

		Connection connection = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String Sql;
		ResultSet resultSet = null;
		statement
				.executeUpdate("Delete from BlockNumber where mobile='" + mobile + "' AND username='" + username + "'");

		connection.close();

	}

}
