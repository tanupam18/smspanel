package com.communications.tubelight.panel.sms.SmsPanel.response;

public class ProfileResponse {
	private String companyName;
	private String ContactNo;
	private String principleEntityId;
	private String Address;
	private String Name;
	private String Role;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactNo() {
		return ContactNo;
	}

	public void setContactNo(String contactNo) {
		ContactNo = contactNo;
	}

	public String getPrincipleEntityId() {
		return principleEntityId;
	}

	public void setPrincipleEntityId(String principleEntityId) {
		this.principleEntityId = principleEntityId;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getRole() {
		return Role;
	}

	public void setRole(String role) {
		Role = role;
	}

	public ProfileResponse() {
		super();
	}

	public ProfileResponse(String companyName, String contactNo, String principleEntityId, String address, String name,
			String role) {
		super();
		this.companyName = companyName;
		ContactNo = contactNo;
		this.principleEntityId = principleEntityId;
		Address = address;
		Name = name;
		Role = role;
	}

	@Override
	public String toString() {
		return "ProfileResponse [companyName=" + companyName + ", ContactNo=" + ContactNo + ", principleEntityId="
				+ principleEntityId + ", Address=" + Address + ", Name=" + Name + ", Role=" + Role + "]";
	}

}
