package com.communications.tubelight.panel.sms.SmsPanel.response;

public class MonthlyStatistic {
	private String date;
	private int submission;
	private int delivered;
	private int undelivered;
	private int expired;
	private int rejected;
	private int dnd;
	private int pending;
	private int other;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getSubmission() {
		return submission;
	}

	public void setSubmission(int submission) {
		this.submission = submission;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public int getUndelivered() {
		return undelivered;
	}

	public void setUndelivered(int undelivered) {
		this.undelivered = undelivered;
	}

	public int getExpired() {
		return expired;
	}

	public void setExpired(int expired) {
		this.expired = expired;
	}

	public int getRejected() {
		return rejected;
	}

	public void setRejected(int rejected) {
		this.rejected = rejected;
	}

	public int getDnd() {
		return dnd;
	}

	public void setDnd(int dnd) {
		this.dnd = dnd;
	}

	public int getPending() {
		return pending;
	}

	public void setPending(int pending) {
		this.pending = pending;
	}

	public int getOther() {
		return other;
	}

	public void setOther(int other) {
		this.other = other;
	}

	public MonthlyStatistic(String date, int submission, int delivered, int undelivered, int expired, int rejected,
			int dnd, int pending, int other) {
		super();
		this.date = date;
		this.submission = submission;
		this.delivered = delivered;
		this.undelivered = undelivered;
		this.expired = expired;
		this.rejected = rejected;
		this.dnd = dnd;
		this.pending = pending;
		this.other = other;
	}

	public MonthlyStatistic() {
		super();
	}

	@Override
	public String toString() {
		return "MonthlyStatistic [date=" + date + ", submission=" + submission + ", delivered=" + delivered
				+ ", undelivered=" + undelivered + ", expired=" + expired + ", rejected=" + rejected + ", dnd=" + dnd
				+ ", pending=" + pending + ", other=" + other + "]";
	}

}
