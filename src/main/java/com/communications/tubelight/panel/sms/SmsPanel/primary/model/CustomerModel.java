package com.communications.tubelight.panel.sms.SmsPanel.primary.model;

public class CustomerModel {
	private int id;
	private String username;
	private String password;
	private String name;
	private String apiKey;

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CustomerModel(int id, String username, String password, String name) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.name = name;
	}

	public CustomerModel(int id, String username, String password, String name, String apiKey) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.name = name;
		this.apiKey = apiKey;
	}

	@Override
	public String toString() {
		return "CustomerModel [id=" + id + ", username=" + username + ", password=" + password + ", name=" + name + "]";
	}

	public CustomerModel() {
		super();
	}

}
