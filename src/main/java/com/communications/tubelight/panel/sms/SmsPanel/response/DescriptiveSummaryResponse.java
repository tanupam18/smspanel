package com.communications.tubelight.panel.sms.SmsPanel.response;

public class DescriptiveSummaryResponse {

	private String dlsStatus;
	private String errorDiscription;
	private String errorCode;
	private int totalCount;

	public String getDlsStatus() {
		return dlsStatus;
	}

	public void setDlsStatus(String dlsStatus) {
		this.dlsStatus = dlsStatus;
	}

	public String getErrorDiscription() {
		return errorDiscription;
	}

	public void setErrorDiscription(String errorDiscription) {
		this.errorDiscription = errorDiscription;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public DescriptiveSummaryResponse(String dlsStatus, String errorDiscription, String errorCode, int totalCount) {
		super();
		this.dlsStatus = dlsStatus;
		this.errorDiscription = errorDiscription;
		this.errorCode = errorCode;
		this.totalCount = totalCount;
	}

	public DescriptiveSummaryResponse() {
		super();
	}

	@Override
	public String toString() {
		return "DescriptiveSummaryResponse [dlsStatus=" + dlsStatus + ", errorDiscription=" + errorDiscription
				+ ", errorCode=" + errorCode + ", totalCount=" + totalCount + "]";
	}

}
