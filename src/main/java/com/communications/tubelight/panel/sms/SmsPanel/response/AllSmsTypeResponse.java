package com.communications.tubelight.panel.sms.SmsPanel.response;

public class AllSmsTypeResponse {

	private String smsType;

	public String getSmsType() {
		return smsType;
	}

	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}

	public AllSmsTypeResponse(String smsType) {
		super();
		this.smsType = smsType;
	}

	public AllSmsTypeResponse() {
		super();
	}

	@Override
	public String toString() {
		return "AllSmsTypeResponse [smsType=" + smsType + "]";
	}

}
