package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class DescriptiveSummaryModel {
	private int size;
	private int page;
	private String search;
	private long fromDate;
	private long toDate;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public long getFromDate() {
		return fromDate;
	}

	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}

	public long getToDate() {
		return toDate;
	}

	public void setToDate(long toDate) {
		this.toDate = toDate;
	}

	public DescriptiveSummaryModel(int size, int page, String search, long fromDate, long toDate) {
		super();
		this.size = size;
		this.page = page;
		this.search = search;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public DescriptiveSummaryModel() {
		super();
	}

	@Override
	public String toString() {
		return "DescriptiveSummaryModel [size=" + size + ", page=" + page + ", search=" + search + ", fromDate="
				+ fromDate + ", toDate=" + toDate + "]";
	}

}
