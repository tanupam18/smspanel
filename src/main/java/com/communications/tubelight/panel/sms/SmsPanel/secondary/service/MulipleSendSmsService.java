package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.MultipleSmsSource;

@Service
public class MulipleSendSmsService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	private int id;
	// private Path root = Paths.get("uploads");

	public void sendMultipleData(MultipleSmsSource multipleSmsSource, String username, String formatted, int SmsLength,
			int CountSms, String metadata) throws SQLException, ClassNotFoundException {

		// System.out.println("root-------------------" +
		// this.root.resolve(multipleSmsSource.getFileName()));

		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		Statement statement;
		statement = connection.createStatement();

		int msgType;
		if (multipleSmsSource.getSmsType().toLowerCase().equals("text")) {
			msgType = 0;
		} else {
			msgType = 1;
		}
		String InsertSql;
		if (multipleSmsSource.getFromDate() != null && !multipleSmsSource.getFromDate().toString().isEmpty()) {
			InsertSql = "INSERT INTO UserJobs(MetaData, TotalNumbers, CampaignName, Username, Message, MessageType,ScheduledAt, Sender, Count, MessageLength,TotalCLicked) "
					+ "VALUES ('" + metadata + "', " + multipleSmsSource.getTotalNumber() + ",'"
					+ multipleSmsSource.getCampaignName() + "','" + username + "','" + multipleSmsSource.getMessage()
					+ "','" + msgType + "' ,'" + formatted + "','" + multipleSmsSource.getSender() + "','" + CountSms
					+ "',' " + SmsLength + "'" + ",'0')";
		} else {

			InsertSql = "INSERT INTO UserJobs(MetaData, TotalNumbers, CampaignName, Username, Message, MessageType, Sender, Count, MessageLength,TotalCLicked) "
					+ "VALUES ('" + metadata + "'," + multipleSmsSource.getTotalNumber() + ",'"
					+ multipleSmsSource.getCampaignName() + "','" + username + "','" + multipleSmsSource.getMessage()
					+ "','" + msgType + "' ,'" + multipleSmsSource.getSender() + "','" + CountSms + "',' " + SmsLength
					+ "'" + ",'0')";
		}

		System.out.println("InsertSql-------------------------" + InsertSql);

		statement.executeUpdate(InsertSql, Statement.RETURN_GENERATED_KEYS);

		ResultSet resultSet;

		try {
			// resultSet = statement.executeQuery("select last_insert_id()");

			resultSet = statement.getGeneratedKeys();

			if (resultSet.next()) {
				this.id = resultSet.getInt(1);
			}

			File f = new File("/home/ftpjobs/" + multipleSmsSource.getFileName() + ".csv");

			if (f.exists()) {
				File rename = new File("/home/ftpjobs/" + this.id + ".csv");
				System.out.println(f.renameTo(rename));

				System.out.println("Exist");
			} else {
				System.out.println("Not Exist");
			}

		} catch (SQLException e) {
			System.out.println(e);
		}

		connection.close();

	}

	public int getJobId() {
		return id;

	}

}
