package com.communications.tubelight.panel.sms.SmsPanel.response;

public class CreditLogResponse {
	
	private String AdjustedAt;
	private String username;
	private String NewCreditAlloted;
	private String OldCreditAlloted;
	private String NewBalance;
	private String OldBalance;
	private String Adjustment;

	public String getAdjustedAt() {
		return AdjustedAt;
	}

	public void setAdjustedAt(String adjustedAt) {
		AdjustedAt = adjustedAt;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNewCreditAlloted() {
		return NewCreditAlloted;
	}

	public void setNewCreditAlloted(String newCreditAlloted) {
		NewCreditAlloted = newCreditAlloted;
	}

	public String getOldCreditAlloted() {
		return OldCreditAlloted;
	}

	public void setOldCreditAlloted(String oldCreditAlloted) {
		OldCreditAlloted = oldCreditAlloted;
	}

	public String getNewBalance() {
		return NewBalance;
	}

	public void setNewBalance(String newBalance) {
		NewBalance = newBalance;
	}

	public String getOldBalance() {
		return OldBalance;
	}

	public void setOldBalance(String oldBalance) {
		OldBalance = oldBalance;
	}

	public String getAdjustment() {
		return Adjustment;
	}

	public void setAdjustment(String adjustment) {
		Adjustment = adjustment;
	}

	public CreditLogResponse(String adjustedAt, String username, String newCreditAlloted, String oldCreditAlloted,
			String newBalance, String oldBalance, String adjustment) {
		super();
		AdjustedAt = adjustedAt;
		this.username = username;
		NewCreditAlloted = newCreditAlloted;
		OldCreditAlloted = oldCreditAlloted;
		NewBalance = newBalance;
		OldBalance = oldBalance;
		Adjustment = adjustment;
	}

	public CreditLogResponse() {
		super();
	}

	@Override
	public String toString() {
		return "CreditLogResponse [AdjustedAt=" + AdjustedAt + ", username=" + username + ", NewCreditAlloted="
				+ NewCreditAlloted + ", OldCreditAlloted=" + OldCreditAlloted + ", NewBalance=" + NewBalance
				+ ", OldBalance=" + OldBalance + ", Adjustment=" + Adjustment + "]";
	}

}
