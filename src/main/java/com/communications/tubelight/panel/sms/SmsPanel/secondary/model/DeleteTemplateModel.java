package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class DeleteTemplateModel {

	private Long templateId;
	private String sender;

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public DeleteTemplateModel(Long templateId, String sender) {
		super();
		this.templateId = templateId;
		this.sender = sender;
	}

	public DeleteTemplateModel() {
		super();
	}

	@Override
	public String toString() {
		return "DeleteTemplateModel [templateId=" + templateId + ", sender=" + sender + "]";
	}

}
