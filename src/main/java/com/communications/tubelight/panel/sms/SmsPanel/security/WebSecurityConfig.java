package com.communications.tubelight.panel.sms.SmsPanel.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.communications.tubelight.panel.sms.SmsPanel.primary.service.UserDetailsServiceImpl;


//import com.communications.tubelight.api.messaging.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		// securedEnabled = true,
		// jsr250Enabled = true,
		prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	UserDetailsServiceImpl userDetailsService;

	@Autowired
	private AuthEntryPointJwt unauthorizedHandler;

	@Bean
	public AuthTokenFilter authenticationJwtTokenFilter() {
		return new AuthTokenFilter();
	}


	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//http.userDetailsService(userDetailsService);
		http.cors().and().csrf().disable()
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.authorizeRequests().antMatchers("/api/authentication/**").permitAll()
			//.authorizeRequests().antMatchers("/sms/api/**").permitAll()

			//.antMatchers("/swagger-ui.**").permitAll()
			
			.anyRequest().authenticated();

		http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	
	 @Override
	    public void configure(WebSecurity web) throws Exception {
	      // Allow swagger to be accessed without authentication
	      web.ignoring().antMatchers("/v2/api-docs")//
	          .antMatchers("/swagger-resources/**")//
	          .antMatchers("/swagger-ui.html")//
	          .antMatchers("/configuration/**")//
	          .antMatchers("/webjars/**")//
	          .antMatchers("/public")
	          .antMatchers("/email.html")
	          .antMatchers("/swagger.json")
	          
	          // Un-secure H2 Database (for testing purposes, H2 console shouldn't be unprotected in production)
	          .and()
	          .ignoring()
	          .antMatchers("/h2-console/**/**");;
	    }
	
//	@Override
//	public void configure(WebSecurity web) throws Exception {
//	    web.ignoring().mvcMatchers(HttpMethod.OPTIONS, "/**");
//	    web.ignoring().mvcMatchers("/swagger-ui.html/**", "/configuration/**", "/swagger-resources/**", "/v2/api-docs","/webjars/**");
//	    web.ignoring().mvcMatchers("/new_swagger-ui.html/**", "/api-docs/**","/email.html/**");
//	  //  web.ignoring().mvcMatchers("/api-docs**");
//
//	}
}