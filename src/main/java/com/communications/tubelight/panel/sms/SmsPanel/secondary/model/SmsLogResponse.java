package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SmsLogResponse {

	private String Source;
	private String destination;
	private int type;
	private String smsType;
	private String Message;
	private int Length;
	private int Count;
	private String Id;
	private String status;
	private String SubmitTime;
	private String DeliveryTime;

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	@JsonIgnore
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getLength() {
		return Length;
	}

	public void setLength(int length) {
		Length = length;
	}

	public int getCount() {
		return Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubmitTime() {
		return SubmitTime;
	}

	public void setSubmitTime(String submitTime) {
		SubmitTime = submitTime;
	}

	public String getDeliveryTime() {
		return DeliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		DeliveryTime = deliveryTime;
	}

	public String getSmsType() {
		return smsType;
	}

	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}

	public SmsLogResponse(String source, String destination, int type, String smsType, String message, int length,
			int count, String id, String status, String submitTime, String deliveryTime) {
		super();
		Source = source;
		this.destination = destination;
		this.type = type;
		this.smsType = smsType;
		Message = message;
		Length = length;
		Count = count;
		Id = id;
		this.status = status;
		SubmitTime = submitTime;
		DeliveryTime = deliveryTime;
	}

	public SmsLogResponse() {
		super();
	}

	@Override
	public String toString() {
		return "SmsLogResponse [Source=" + Source + ", destination=" + destination + ", type=" + type + ", smsType="
				+ smsType + ", Message=" + Message + ", Length=" + Length + ", Count=" + Count + ", Id=" + Id
				+ ", status=" + status + ", SubmitTime=" + SubmitTime + ", DeliveryTime=" + DeliveryTime + "]";
	}

}
