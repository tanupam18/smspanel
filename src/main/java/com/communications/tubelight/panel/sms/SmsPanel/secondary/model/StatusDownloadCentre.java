package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class StatusDownloadCentre {

	private boolean pending;
	private boolean rejected;
	private boolean delivery;

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isDelivery() {
		return delivery;
	}

	public void setDelivery(boolean delivery) {
		this.delivery = delivery;
	}

	public StatusDownloadCentre(boolean pending, boolean rejected, boolean delivery) {
		super();
		this.pending = pending;
		this.rejected = rejected;
		this.delivery = delivery;
	}

	public StatusDownloadCentre() {
		super();
	}

	@Override
	public String toString() {
		return "StatusDownloadCentre [pending=" + pending + ", rejected=" + rejected + ", delivery=" + delivery + "]";
	}

}
