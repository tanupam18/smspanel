package com.communications.tubelight.panel.sms.SmsPanel.response;

public class DataStatisticResponse {
	private String sender;
	private String date;
	private String status;
	private int count;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public DataStatisticResponse(String sender, String date, String status, int count) {
		super();
		this.sender = sender;
		this.date = date;
		this.status = status;
		this.count = count;
	}

	public DataStatisticResponse() {
		super();
	}

	@Override
	public String toString() {
		return "DateStatisticModel [sender=" + sender + ", date=" + date + ", status=" + status + ", count=" + count
				+ "]";
	}

}
