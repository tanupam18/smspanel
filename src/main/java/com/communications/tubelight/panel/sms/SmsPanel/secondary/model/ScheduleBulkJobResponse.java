package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class ScheduleBulkJobResponse {

	private Long Summary;
	private String FileName;
	private String type;
	private String message;
	private int length;
	private String sender;
	private int Tcount;
	private String QueuedAt;
	private String CompletedAt;
	private int Tsent;
	private int Status;
	private String StatusType;
	private String Action;
	private boolean pause;
	private boolean resume;
	private boolean delete;

	public Long getSummary() {
		return Summary;
	}

	public void setSummary(Long summary) {
		Summary = summary;
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public int getTcount() {
		return Tcount;
	}

	public void setTcount(int tcount) {
		Tcount = tcount;
	}

	public String getQueuedAt() {
		return QueuedAt;
	}

	public void setQueuedAt(String queuedAt) {
		QueuedAt = queuedAt;
	}

	public String getCompletedAt() {
		return CompletedAt;
	}

	public void setCompletedAt(String completedAt) {
		CompletedAt = completedAt;
	}

	public int getTsent() {
		return Tsent;
	}

	public void setTsent(int tsent) {
		Tsent = tsent;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getStatusType() {
		return StatusType;
	}

	public void setStatusType(String statusType) {
		StatusType = statusType;
	}

	public String getAction() {
		return Action;
	}

	public void setAction(String action) {
		Action = action;
	}

	public boolean isPause() {
		return pause;
	}

	public void setPause(boolean pause) {
		this.pause = pause;
	}

	public boolean isResume() {
		return resume;
	}

	public void setResume(boolean resume) {
		this.resume = resume;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public ScheduleBulkJobResponse(Long summary, String fileName, String type, String message, int length,
			String sender, int tcount, String queuedAt, String completedAt, int tsent, int status, String statusType,
			String action, boolean pause, boolean resume, boolean delete) {
		super();
		Summary = summary;
		FileName = fileName;
		this.type = type;
		this.message = message;
		this.length = length;
		this.sender = sender;
		Tcount = tcount;
		QueuedAt = queuedAt;
		CompletedAt = completedAt;
		Tsent = tsent;
		Status = status;
		StatusType = statusType;
		Action = action;
		this.pause = pause;
		this.resume = resume;
		this.delete = delete;
	}

	public ScheduleBulkJobResponse() {
		super();
	}

	@Override
	public String toString() {
		return "ScheduleBulkJobResponse [Summary=" + Summary + ", FileName=" + FileName + ", type=" + type
				+ ", message=" + message + ", length=" + length + ", sender=" + sender + ", Tcount=" + Tcount
				+ ", QueuedAt=" + QueuedAt + ", CompletedAt=" + CompletedAt + ", Tsent=" + Tsent + ", Status=" + Status
				+ ", StatusType=" + StatusType + ", Action=" + Action + ", pause=" + pause + ", resume=" + resume
				+ ", delete=" + delete + "]";
	}

}
