package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.RetreiveDownloadCenterResponse;

@Service
public class DownloadCenterCsvService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	@Value("${spring.download.link}")
	private String link;

	public List<RetreiveDownloadCenterResponse> downloadRetreivingCenter(String username) throws SQLException {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;

		List<RetreiveDownloadCenterResponse> list = null;

		Sql = "Select * from  DownloadLogs where username='" + username + "'";

		try {
			resultSet = statement.executeQuery(Sql);
			// System.out.println("---------------------------" + resultSet.getFetchSize());
		} catch (SQLException e) {
			System.out.println(e);
		}
		list = new ArrayList<RetreiveDownloadCenterResponse>();

		while (resultSet.next()) {
			RetreiveDownloadCenterResponse retreiveDownloadCenterResponse = new RetreiveDownloadCenterResponse();
			retreiveDownloadCenterResponse.setDownloadId(resultSet.getString("UUID"));
			// System.out.println("Uuid-----------------" + resultSet.getString("UUID"));

			retreiveDownloadCenterResponse.setUsername(resultSet.getString("username"));
			// System.out.println("Username-----------------" +
			// resultSet.getString("username"));

			retreiveDownloadCenterResponse.setSource(resultSet.getString("sender"));
			// System.out.println("sender-----------------" +
			// resultSet.getString("sender"));

			retreiveDownloadCenterResponse.setFromDate(resultSet.getString("From"));
			// System.out.println("FromDate-----------------" +
			// resultSet.getString("From"));

			retreiveDownloadCenterResponse.setToDate(resultSet.getString("To"));
			// System.out.println("ToDate-------------------" + resultSet.getString("To"));

			retreiveDownloadCenterResponse.setDataCount(resultSet.getString("DataCount"));

			retreiveDownloadCenterResponse.setRequestTime(resultSet.getString("RequestTime"));
			if (resultSet.getInt("status") == 0) {
				// System.out.println("Status-------------------" + resultSet.getInt("status"));

				retreiveDownloadCenterResponse.setStatus("Queued");
			} else if (resultSet.getInt("status") == 1) {
				retreiveDownloadCenterResponse.setStatus("Counting");
			} else if (resultSet.getInt("status") == 2) {
				retreiveDownloadCenterResponse.setStatus("Fetching");
			} else if (resultSet.getInt("status") == 4) {
				retreiveDownloadCenterResponse.setStatus("Completed");
				retreiveDownloadCenterResponse.setLink(link);

			}

			list.add(retreiveDownloadCenterResponse);

		}

		try {
			resultSet.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		connection.close();
		return list;

	}

}
