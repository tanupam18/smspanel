package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class MatchTemplateModel {

	private String sender;
	private String message;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MatchTemplateModel(String sender, String message) {
		super();
		this.sender = sender;
		this.message = message;
	}

	public MatchTemplateModel() {
		super();
	}

	@Override
	public String toString() {
		return "MatchTemplateModel [sender=" + sender + ", message=" + message + "]";
	}

}
