package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class UploadResponseMessage {
	private String message;
	private String filename;

	public UploadResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public UploadResponseMessage(String message, String filename) {
		super();
		this.message = message;
		this.filename = filename;
	}

	@Override
	public String toString() {
		return "UploadResponseMessage [message=" + message + ", filename=" + filename + "]";
	}

}