package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.DescriptiveSummaryResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.SenderStatsticResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.BulkJobResponse;

@Service
public class DiscriptiveSummaryService {

	private int noOfRecords;

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<DescriptiveSummaryResponse> discriptiveSummary(int offset, int noOfRecords, String search,
			String username, String fromDate, String toDate) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;

		List<DescriptiveSummaryResponse> list = null;

		if (search != null && !search.isEmpty()) {
			Sql = "Select s.DlrStatus, g.ErrorDiscription, s.ErrorCode, SUM(Count) as totalcount FROM Summary AS s, Main.GatewayErrorDescription AS g WHERE g.ErrorCode = s.ErrorCode AND s.Username = '"
					+ username + "' \n" + "AND DateTime between '" + fromDate + "' and '" + toDate
					+ "' AND s.DlrStatus like '%" + search + "%' OR g.ErrorDiscription like '%" + search
					+ "%' OR g.ErrorCode like '%" + search
					+ "%'  GROUP BY s.DlrStatus, s.ErrorCode ORDER BY DateTime DESC limit " + noOfRecords;

		} else {
			Sql = "Select s.DlrStatus, g.ErrorDiscription, s.ErrorCode, SUM(Count) as totalcount \n"
					+ "FROM tube_Logs.Summary AS s, Main.GatewayErrorDescription AS g WHERE g.ErrorCode = s.ErrorCode AND s.Username = '"
					+ username + "' \n" + "AND DateTime between '" + fromDate + "' and '" + toDate
					+ "' GROUP BY s.DlrStatus, s.ErrorCode ORDER BY DateTime DESC limit " + offset + ", " + noOfRecords;
		}
		System.out.println("SQL---------------------------------------" + Sql);
		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			System.out.println(e);
		}
		list = new ArrayList<DescriptiveSummaryResponse>();

		try {

			while (resultSet.next()) {
				DescriptiveSummaryResponse descriptiveSummaryResponse = new DescriptiveSummaryResponse();

				descriptiveSummaryResponse.setDlsStatus(resultSet.getString("s.DlrStatus"));
				descriptiveSummaryResponse.setErrorDiscription(resultSet.getString("g.ErrorDiscription"));
				descriptiveSummaryResponse.setErrorCode(resultSet.getString("s.ErrorCode"));
				descriptiveSummaryResponse.setTotalCount(resultSet.getInt("totalcount"));

				list.add(descriptiveSummaryResponse);

			}
			resultSet.close();

			resultSet = statement
					.executeQuery("Select s.DlrStatus, g.ErrorDiscription, s.ErrorCode, SUM(Count) as totalcount \n"
							+ "FROM tube_Logs.Summary AS s, Main.GatewayErrorDescription AS g WHERE g.ErrorCode = s.ErrorCode AND s.Username = '"
							+ username + "' \n" + "AND DateTime between '" + fromDate + "' and '" + toDate
							+ "' GROUP BY s.DlrStatus, s.ErrorCode ORDER BY DateTime DESC");

			int count = 0;

			while (resultSet.next()) {
				count++;
			}
			System.out.println("---------Count Descriptive------        "+"Select s.DlrStatus, g.ErrorDiscription, s.ErrorCode, SUM(Count) as totalcount \n"
							+ "FROM tube_Logs.Summary AS s, Main.GatewayErrorDescription AS g WHERE g.ErrorCode = s.ErrorCode AND s.Username = '"
							+ username + "' \n" + "AND DateTime between '" + fromDate + "' and '" + toDate
							+ "' GROUP BY s.DlrStatus, s.ErrorCode ORDER BY DateTime DESC");
			System.out.println("count--------------------"+count);
			this.noOfRecords = count;

		} catch (Exception e) {
			System.out.println("e----------" + e);

		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;

	}

	public int getNoOfRecords() {
		return noOfRecords;
	}
}
