//package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;
//
//import java.util.Map;
//
//public class DownloadCentreModel {
//
//	private long fromDate;
//	private long toDate;
//	private String MobileNumber;
//	private String jobId;
//	private ColumnDownloadCentre Columns;
//	private String sender;
//	private StatusDownloadCentre status;
//
//	private int page;
//	private int size;
//	private String Search;
//
//	public int getPage() {
//		return page;
//	}
//
//	public void setPage(int page) {
//		this.page = page;
//	}
//
//	public int getSize() {
//		return size;
//	}
//
//	public void setSize(int size) {
//		this.size = size;
//	}
//
//	public String getSearch() {
//		return Search;
//	}
//
//	public void setSearch(String search) {
//		Search = search;
//	}
//
//	public long getFromDate() {
//		return fromDate;
//	}
//
//	public void setFromDate(long fromDate) {
//		this.fromDate = fromDate;
//	}
//
//	public long getToDate() {
//		return toDate;
//	}
//
//	public void setToDate(long toDate) {
//		this.toDate = toDate;
//	}
//
//	public String getMobileNumber() {
//		return MobileNumber;
//	}
//
//	public void setMobileNumber(String mobileNumber) {
//		MobileNumber = mobileNumber;
//	}
//
//	public String getJobId() {
//		return jobId;
//	}
//
//	public void setJobId(String jobId) {
//		this.jobId = jobId;
//	}
//
//	public ColumnDownloadCentre getColumns() {
//		return Columns;
//	}
//
//	public void setColumns(ColumnDownloadCentre columns) {
//		Columns = columns;
//	}
//
//	public String getSender() {
//		return sender;
//	}
//
//	public void setSender(String sender) {
//		this.sender = sender;
//	}
//
//	public StatusDownloadCentre getStatus() {
//		return status;
//	}
//
//	public void setStatus(StatusDownloadCentre status) {
//		this.status = status;
//	}
//
//	public DownloadCentreModel(long fromDate, long toDate, String mobileNumber, String jobId,
//			ColumnDownloadCentre columns, String sender, StatusDownloadCentre status, int page, int size,
//			String search) {
//		super();
//		this.fromDate = fromDate;
//		this.toDate = toDate;
//		MobileNumber = mobileNumber;
//		this.jobId = jobId;
//		Columns = columns;
//		this.sender = sender;
//		this.status = status;
//		this.page = page;
//		this.size = size;
//		Search = search;
//	}
//
//	public DownloadCentreModel() {
//		super();
//	}
//
//	@Override
//	public String toString() {
//		return "DownloadCentreModel [fromDate=" + fromDate + ", toDate=" + toDate + ", MobileNumber=" + MobileNumber
//				+ ", jobId=" + jobId + ", Columns=" + Columns + ", sender=" + sender + ", status=" + status + ", page="
//				+ page + ", size=" + size + ", Search=" + Search + "]";
//	}
//
//}
