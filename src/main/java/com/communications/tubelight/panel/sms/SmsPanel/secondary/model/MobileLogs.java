package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class MobileLogs {
	
	private int fromDate;
	private int toDate;
	private String mobileNo;

	public int getFromDate() {
		return fromDate;
	}

	public void setFromDate(int fromDate) {
		this.fromDate = fromDate;
	}

	public int getToDate() {
		return toDate;
	}

	public void setToDate(int toDate) {
		this.toDate = toDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public MobileLogs(int fromDate, int toDate, String mobileNo) {
		super();
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.mobileNo = mobileNo;
	}

	public MobileLogs() {
		super();
	}

	@Override
	public String toString() {
		return "MobileLogs [fromDate=" + fromDate + ", toDate=" + toDate + ", mobileNo=" + mobileNo + "]";
	}

}
