package com.communications.tubelight.panel.sms.SmsPanel.primary.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.communications.tubelight.panel.sms.SmsPanel.primary.entity.User;

//@Repository
//public interface UserRepository extends JpaRepository<User, Long> {
//	Optional<User> findByUsername(String username);
//
//	@Query(value = "select username from users where id =:id", nativeQuery = true)
//	
//	String findNameById(@Param("id") Long id);
//
//	//Optional<User> findById(Integer id);
//
//}

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);

	
	
//	@Query(value = "select username from Customers where id =:id", nativeQuery = true)
//	
//	String findNameById(@Param("id") Long id);

	//Optional<User> findById(Integer id);

}
