package com.communications.tubelight.panel.sms.SmsPanel.primary.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.communications.tubelight.panel.sms.SmsPanel.primary.entity.User;
import com.communications.tubelight.panel.sms.SmsPanel.primary.model.CustomerModel;
import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.UserRepository;
import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.UserRepositoryService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserRepositoryService repositoryService;

//	@Override
//	@Transactional
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		User user = userRepository.findByUsername(username)
//				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
//		//System.out.println("user-----------------------"+user.getPassword()+" , "+user.getUsername());
//		//System.out.println("SQL--------------------"+"select username from users where id =:id");
//
//		return UserDetailsImpl.build(user);
//	}
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		CustomerModel user=null;
		try {
			user = repositoryService.findByUsername(username);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		//System.out.println("user-----------------------"+user.getPassword()+" , "+user.getUsername());
		//System.out.println("SQL--------------------"+"select username from users where id =:id");

		return UserDetailsImpl.build(user);
	}

}