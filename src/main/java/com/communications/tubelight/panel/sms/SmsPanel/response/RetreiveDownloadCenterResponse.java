package com.communications.tubelight.panel.sms.SmsPanel.response;

public class RetreiveDownloadCenterResponse {

	private String DownloadId;
	private String username;
	private String Source;
	private String fromDate;
	private String toDate;
	private String dataCount;
	private String RequestTime;
	private String status;
	private String link;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDownloadId() {
		return DownloadId;
	}

	public void setDownloadId(String downloadId) {
		DownloadId = downloadId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDataCount() {
		return dataCount;
	}

	public void setDataCount(String dataCount) {
		this.dataCount = dataCount;
	}

	public String getRequestTime() {
		return RequestTime;
	}

	public void setRequestTime(String requestTime) {
		RequestTime = requestTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public RetreiveDownloadCenterResponse(String downloadId, String username, String source, String fromDate,
			String toDate, String dataCount, String requestTime, String status, String link) {
		super();
		DownloadId = downloadId;
		this.username = username;
		Source = source;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.dataCount = dataCount;
		RequestTime = requestTime;
		this.status = status;
		this.link = link;
	}

	public RetreiveDownloadCenterResponse() {
		super();
	}

	@Override
	public String toString() {
		return "RetreiveDownloadCenterResponse [DownloadId=" + DownloadId + ", username=" + username + ", Source="
				+ Source + ", fromDate=" + fromDate + ", toDate=" + toDate + ", dataCount=" + dataCount
				+ ", RequestTime=" + RequestTime + ", status=" + status + ", link=" + link + "]";
	}

}
