package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SmsLogResponse;

@Service
public class MobileLogDownloadService {

	private int noOfRecords;

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<SmsLogResponse> mobileLogs(String fromDateformatted, String mobileNumber, String username) throws SQLException {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;
		List<SmsLogResponse> list = null;
		String DBLogs = null;
		Timestamp MonthPendingSms = null;

		String DbLogsCustomerTable = "Select LogsDB from Main.Customers where  username = '" + username + "' limit 1;";
		System.out.println("DbLogs--------------"+DbLogsCustomerTable);

		try {
			resultSet = statement.executeQuery(DbLogsCustomerTable);
			if (resultSet.next())
				DBLogs = resultSet.getString("LogsDB");
			resultSet.close();

			String MonthonPendingSms = "SELECT SentLogTime FROM " + DBLogs + ".PendingSms where Username= '" + username
					+ "' \n" + "and SentLogTime Between '" + fromDateformatted + " 00:00:00' And '" + fromDateformatted
					+ " 23:59:59' AND Destination ='" + mobileNumber + "' limit 1 ";
			System.out.println("MonthonPendingSms============================"+MonthonPendingSms);

			resultSet = statement.executeQuery(MonthonPendingSms);

			if (resultSet.next())
				MonthPendingSms = resultSet.getTimestamp("SentLogTime");
			System.out.println("MonthPendingSms----------------"+MonthPendingSms);
			resultSet.close();

			Calendar cal = Calendar.getInstance();
			String SmsTable = null;

			try {
				Format FormatDateString = new SimpleDateFormat("dd/MMMM/yyyy");
				FormatDateString = new SimpleDateFormat("yyyy");
				String SmsYear = FormatDateString.format(MonthPendingSms);
				
				System.out.println("Year-------------"+ SmsYear);
				
				FormatDateString = new SimpleDateFormat("MMM");
				

				String SmsMonth = FormatDateString.format(MonthPendingSms);
				
				System.out.println("Month-------------"+ SmsMonth);


				SmsTable = DBLogs + ".Sms_" + SmsMonth + SmsYear;
				System.out.println(SmsTable);

			} catch (Exception e) {
				System.out.println(e);
			}

			String Sql;

			try {
				Sql = "Select Source,Destination,MsgType,Message,MsgLength,MsgCount,UUID,DeliveryStatus,SentLogTime, DlrLoggedTime \n"
						+ "FROM " + DBLogs + ".PendingSms where Username= '" + username + "' and SentLogTime Between '"
						+ fromDateformatted + " 00:00:00' And '" + fromDateformatted + " 23:59:59' \n" + "AND Destination = '" + mobileNumber
						+ "' UNION SELECT Source,Destination,MsgType,Message,MsgLength,MsgCount,\n"
						+ "UUID,DeliveryStatus,SentLogTime, DlrLoggedTime FROM " + SmsTable + " where Username= '"
						+ username + "' \n" + "and SentLogTime Between '" + fromDateformatted + " 00:00:00' And '" + fromDateformatted
						+ " 23:59:59' AND Destination = '" + mobileNumber + "'";

				System.out.println(Sql);
				resultSet = statement.executeQuery(Sql);

				list = new ArrayList<SmsLogResponse>();

				while (resultSet.next()) {

					SmsLogResponse logResponse = new SmsLogResponse();
					logResponse.setSource(resultSet.getString("source"));
					logResponse.setDestination(resultSet.getString("destination"));

					if (resultSet.getInt("msgtype") == 0) {
						logResponse.setSmsType("TEXT");
					} else {
						logResponse.setSmsType("UNICODE");

					}
					logResponse.setMessage(resultSet.getString("Message"));
					logResponse.setLength(resultSet.getInt("msglength"));
					logResponse.setCount(resultSet.getInt("msgcount"));
					logResponse.setId(resultSet.getString("uuid"));
					logResponse.setStatus(resultSet.getString("DeliveryStatus"));
					logResponse.setSubmitTime(resultSet.getString("SentLogTime"));
					logResponse.setDeliveryTime(resultSet.getString("DlrLoggedTime"));

					list.add(logResponse);
				}

			} catch (Exception e) {
				System.out.println(e);
			}

		} catch (SQLException e) {
			System.out.println(e);
		}

		connection.close();
		return list;

	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

}
