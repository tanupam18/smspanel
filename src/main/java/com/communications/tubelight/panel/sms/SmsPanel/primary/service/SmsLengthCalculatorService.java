package com.communications.tubelight.panel.sms.SmsPanel.primary.service;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class SmsLengthCalculatorService {

    public static final int GSM_CHARSET_7BIT = 0;
    public static final int GSM_CHARSET_UNICODE = 2;
    private static final char GSM_7BIT_ESC = '\u001b';

    private  final Set<String> GSM7BIT = new HashSet<String>(Arrays.asList(
            new String[]{
                "@", "£", "$", "¥", "è", "é", "ù", "ì", "ò", "Ç", "\r\n", "Ø", "ø", "\r", "Å", "å",
                "Δ", "_", "Φ", "Γ", "Λ", "Ω", "Π", "Ψ", "Σ", "Θ", "Ξ", "\u001b", "Æ", "æ", "ß", "É",
                " ", "!", "'", "#", "¤", "%", "&", "\"", "(", ")", "*", "+", ",", "-", ".", "/",
                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?",
                "¡", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Ä", "Ö", "Ñ", "Ü", "§",
                "¿", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
                "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "ä", "ö", "ñ", "ü", "à"
            }
    ));

    private  final Set<String> GSM7BITEXT = new HashSet<String>(Arrays.asList(
            new String[]{
                "\f", "^", "{", "}", "\\", "[", "~", "]", "|", "€"
            }
    ));

    public int getCharset(String content) {

        for (int i = 0; i < content.length(); i++) {
            if (!GSM7BIT.contains(Character.toString(content.charAt(i)))) {
                if (!GSM7BITEXT.contains(Character.toString(content.charAt(i)))) {
                    return GSM_CHARSET_UNICODE;
                }
            }
        }

        return GSM_CHARSET_7BIT;

    }

    public  int getSmsLength(String content) {

        StringBuilder content7bit = new StringBuilder();

        // Add escape characters for extended charset
        for (int i = 0; i < content.length(); i++) {
            if (!GSM7BITEXT.contains(content.charAt(i) + "")) {
                content7bit.append(content.charAt(i));
            } else {
                content7bit.append('\u001b');
                content7bit.append(content.charAt(i));
            }
        }

        return content7bit.length();

    }

    public static void main(String[] args) {
//        String content = "जय जिनेन्द्र,\r\n"
//                + "\r\n"
//                + "JIO दे रहा है एक स्वर्णिम अवसर\r\n"
//                + "\r\n"
//                + "आपका सम इन्शुरड बढ़ाने का\r\n"
//                + "\r\n"
//                + "आदित्य बिरला इन्शुरन्स कंपनी से\r\n"
//                + "\r\n"
//                + "आपकी पर्सनल, ग्रुप, फॅमिली फ्लोटर या JIO मेडिक्लेम पालिसी को टॉप अप कराएं\r\n"
//                + "\r\n"
//                + "क्योंकि आपकी पॉलिसी आपको को-पे, कैपिंग और रूम रेंट की रकम काटके\r\n"
//                + "\r\n"
//                + "आपका मेडिक्लेम पॉलिसी का पूरा सम इन्शुरड आपको शायद नहीं देगी\r\n"
//                + "\r\n"
//                + "सम इन्शुरड पर्याय:\r\n"
//                + "\r\n"
//                + "रु २ लाख,\r\n"
//                + "\r\n"
//                + "रु ५ लाख\r\n"
//                + "\r\n"
//                + "रु १० लाख \r\n"
//                + "\r\n"
//                + "और पाए पुरे अस्पताल के खर्च का भुगतान |\r\n"
//                + "\r\n"
//                + "# पुरानी बीमारियां पहले दिन से कवर\r\n"
//                + "\r\n"
//                + "# कोई को पे, कैपिंग और वेटिंग पीरियड नहीं\r\n"
//                + "\r\n"
//                + "# 85 वर्ष तक बिना मेडिकल चेक अप\r\n"
//                + "\r\n"
//                + "विस्तृत जानकारी\r\n"
//                + "\r\n"
//                + "180030103360\r\n"
//                + "\r\n"
//                + "अंतिम तिथि\r\n"
//                + "\r\n"
//                + "31-मार्च-2018\r\n"
//                + "\r\n"
//                + "www.jiojac.com/SuperTopUp";

        //int smsLength = getSmsLength(content);
//       int smsLength = 130;
//        if (smsLength > 70) {
//            int count = smsLength / 67 + 1;
//            System.out.println(count);
//        }

        //System.out.println((content));
    }

    private int getPartCount7bit(String content) {

        StringBuilder content7bit = new StringBuilder();

        // Add escape characters for extended charset
        for (int i = 0; i < content.length(); i++) {
            if (!GSM7BITEXT.contains(content.charAt(i) + "")) {
                content7bit.append(content.charAt(i));
            } else {
                content7bit.append('\u001b');
                content7bit.append(content.charAt(i));
            }
        }

        if (content7bit.length() <= 160) {

            return 1;

        } else {

            // Start counting the number of messages
            int parts = (int) Math.ceil(content7bit.length() / 153.0);
            int free_chars = content7bit.length() - (int) Math.floor(content7bit.length() / 153.0) * 153;

            // We have enough free characters left, don't care about escape character at the end of sms part
            if (free_chars >= parts - 1) {
                return parts;
            }

            // Reset counter
            parts = 0;
            while (content7bit.length() > 0) {

                // Advance sms counter
                parts++;

                // Check for trailing escape character
                if (content7bit.length() >= 152 && content7bit.charAt(152) == GSM_7BIT_ESC) {
                    content7bit.delete(0, 152);
                } else {
                    content7bit.delete(0, 153);
                }

            }

            return parts;

        }

    }

    public int getPartCount(String content) {

        int charset = getCharset(content);

        if (charset == GSM_CHARSET_7BIT) {

            return this.getPartCount7bit(content);

        } else if (charset == GSM_CHARSET_UNICODE) {

            if (content.length() <= 70) {
                return 1;
            } else {
                return (int) Math.ceil(content.length() / 67.0);
            }

        }

        return -1;

    } // getPartCount

} // SmsLengthCalculator