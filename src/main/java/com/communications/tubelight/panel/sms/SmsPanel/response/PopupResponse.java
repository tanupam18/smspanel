package com.communications.tubelight.panel.sms.SmsPanel.response;

public class PopupResponse {

	private String date;
	private int id;
	private String message;
	private int MessageCount;
	private int messageType;
	private String mask;

	private String status;
	private int count;
	private int parts;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getMessageCount() {
		return MessageCount;
	}

	public void setMessageCount(int messageCount) {
		MessageCount = messageCount;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public String getMask() {
		return mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getParts() {
		return parts;
	}

	public void setParts(int parts) {
		this.parts = parts;
	}

	public PopupResponse(String date, int id, String message, int messageCount, int messageType, String mask,
			String status, int count, int parts) {
		super();
		this.date = date;
		this.id = id;
		this.message = message;
		MessageCount = messageCount;
		this.messageType = messageType;
		this.mask = mask;
		this.status = status;
		this.count = count;
		this.parts = parts;
	}

	public PopupResponse() {
		super();
	}

	@Override
	public String toString() {
		return "PopupResponse [date=" + date + ", id=" + id + ", message=" + message + ", MessageCount=" + MessageCount
				+ ", messageType=" + messageType + ", mask=" + mask + ", status=" + status + ", count=" + count
				+ ", parts=" + parts + "]";
	}

}
