package com.communications.tubelight.panel.sms.SmsPanel.primary.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.communications.tubelight.panel.sms.SmsPanel.primary.entity.RefreshTokenEntity;
import com.communications.tubelight.panel.sms.SmsPanel.primary.entity.User;


public interface RefreshTokenRepository extends  JpaRepository<RefreshTokenEntity, Long>  {


    Optional<RefreshTokenEntity> findByToken(String token);

	Optional<RefreshTokenEntity> deleteAllById(Long userId);

	int deleteByUser(User user);
	
	@Query(value="select token from refreshtoken r inner join users u on r.user_id = u.id And u.id=:id", nativeQuery = true)
	String findTokenById(Long id);
	
	@Query(value="select user_id from refreshtoken where token=:token", nativeQuery = true)
	Long findIdByRefreshToken(String token);

}
